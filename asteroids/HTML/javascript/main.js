const X = 0, Y = 1
//const width = $('#board').width()
const width = 600
//const height = $('#board').height()
const height = 600
const speedMAX = 4

var up, left, right

$(document).ready( function () {
	
	//$(document).keypress( function(event) { getKey (event.key) })
	$(document).keydown( function(event) { setKey (event.key, true ) })
	$(document).keyup( function(event) { setKey (event.key, false) })

	canvas = document.getElementById('board').getContext('2d')
})

// Principal cycle
setInterval ( function () {
	getKey ()
	updateVar ()
	draw ()
}, 10)

class Objects {
	constructor (pos, speed, angle, type) {
		this.pos = pos
		this.speed = speed
		this.angle = angle
		this.type = type	// Ship = 0, Misile = 1, Asteroid = 2
	}

	move () {
		switch (this.type) {
			case 0:
				this.setSpeedMax (X)
				this.setSpeedMax (Y)

				this.pos[X] += this.speed[X]
				this.pos[Y] += this.speed[Y]

				break

			case 1:
			case 2:
				this.pos[X] += this.speed[X] * Math.sin(this.angle)
				this.pos[Y] += this.speed[Y] * Math.cos(this.angle)

				break
		}

		this.borders (X, width)
		this.borders (Y, height)
	}

	setSpeed (x, y) {
		this.speed[X] += x * Math.sin(this.angle)
		this.speed[Y] += y * Math.cos(this.angle)
	}

	setSpeedMax (p) {
		if (this.speed[p] > speedMAX)
			this.speed[p] = speedMAX
		else if (this.speed[p] < -speedMAX)
			this.speed[p] = -speedMAX
	}

	borders (p, end) {
		if ( Math.round(this.pos[p]) < 0 )
			this.pos[p] = end
		else if ( Math.round(this.pos[p]) > end )
			this.pos[p] = 0
	}
}

var canvas
var ship, misile

ship = new Objects([300, 300], [0, 0], 0, 0)
misile = new Array()

/* KEY FUNCTIONS */
function setKey (key, bool) {
	switch (key) {
		case "ArrowUp":
			up = bool
			break
		case "ArrowLeft":
			left = bool
			break
		case "ArrowRight":
			right = bool
			break
		case " ":
			if (bool)
				createMisile()
			break
	}
}

function getKey () {

	if (up)
		ship.setSpeed (1, -1)

	if (left)
		ship.angle -= 0.05

	if (right)
		ship.angle += 0.05
}

/* MOVE FUNCTION */
function updateVar () {

	ship.move()

	for (let i = 0; i < misile.length; i++) {
		misile[i].move()
	}
}

function createMisile () {
	if (misile.length == 10)
		misile.shift()
	
	misile.push( 
		new Objects ( 
			[ship.pos[X], ship.pos[Y]],
			[ship.speed[X] * 2, ship.speed[Y] * 2],
			ship.angle, 1)
	)
}


/*
var canvas = null

var pos = []
var angle = 0
var moveAngle = 0
var speed = 0

/*******************************************/
/*	
/*	Para hacer el movimiento tengo X e Y
/*	Como Max X = 3 = Y
/*	Y cada vez que presionas una key se suma	
/* o se resta pero como X +-= 0.5 =-+ Y
/*
/*	Para hacer el disparo solo lee la X e Y
/*
/*******************************************/

/*
var X = 0

var key = [
	new createKey ("ArrowUp", 	 0.2),
    new createKey ("ArrowLeft", -3),
    new createKey ("ArrowRight", 3),
    new createKey ("ArrowDown",  0.5),
    new createKey (" ", 0)
]
var objects = [
	new createObject (3, 100, 100, 0.5, 1.2, 0.5),
	new createObject (3, 400, 400,   1, 0.2, 1.5),
	new createObject (2, 400,   0, 0.2,   1,   1),
	new createObject (2, 200,  40,   2, 0.7,   1),
	new createObject (1, 420, 350,   0, 0.8,   2),
	new createObject (1, 340, 560, 0.9, 0.1, 0.2)
]

function createKey (type, num) {
	this.type = type
    this.bool = false
    this.num = num
}

function createObject (size, x, y, dirX, dirY, moveAngle) {
	this.size = size * 10
	this.x = x
	this.y = y
	this.dirX = dirX
	this.dirY = dirY
	this.angle = 0// (Math.random() * 360) * Math.PI / 180
	this.moveAngle = moveAngle
} 

setInterval (cycle, 10)

function drawGame() {
	drawFace ()
	drawObjects ()
	// drawScore ()
}

function updateVariables () {

	ship ()
	misile ()
	moveObjects ()
	getID('demo').innerHTML = hit (pos[0], pos[1], objects[0].x - objects[0].size/2, objects[0].y - objects[0].size/2) + " --- " + objects[0].size +
		"<br>" + objects[0].x + " - " + objects[0].y
	if ( hit (pos[0], pos[1], objects[0].x - objects[0].size/2, objects[0].y - objects[0].size/2) <= objects[0].size-1)
		alert('MayDay MayDay BlackHock derribado!!')
}

function cycle () {
	updateVariables ()
	drawGame ()
}

function main () {

	canvas = getID('board').getContext('2d')

	width = getID('board').width
	height = getID('board').height

	window.addEventListener('keydown', function (e) {
		for (var i = 0; i < key.length; i++)
			if ( e.key == key[i].type )
				key[i].bool = true
	})
	window.addEventListener('keyup', function (e) {
		for (var i = 0; i < key.length; i++)
			if ( e.key == key[i].type )
				key[i].bool = false
	})

	pos = [width/2, height/2]

	drawGame()
}
*/