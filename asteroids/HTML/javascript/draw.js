function draw () {

	drawBackground ()
	drawShip ()
	drawObjects ()
	drawScore ()
}

function drawBackground () {

	// Clear and paint the background
	canvas.beginPath()
	canvas.clearRect(0, 0, width, height)
	canvas.fillStyle = "black"
	canvas.rect(0, 0, width, height)
	canvas.fill()	

	// Background
	canvas.beginPath()
	canvas.fillStyle = "white"
	for (let i = 0; i < 200; i++) {
		canvas.fillRect(Math.random() * width, Math.random() * height, 1, 1)
	}
	canvas.fill()
}

function drawShip () {
	// Ship
	canvas.strokeStyle = "#FFFFFF"
	canvas.fillStyle = "#000000"
	canvas.save()
	canvas.beginPath()
	canvas.translate(ship.pos[X], ship.pos[Y])
	canvas.rotate(ship.angle)
	// Rotation point
	canvas.moveTo(0, 0)
	canvas.lineTo(-5, 6)
	canvas.lineTo( 0, -11)
	canvas.lineTo( 5, 6)
	canvas.closePath()
	canvas.restore()
	canvas.stroke()
	canvas.fill()

}

function drawObjects () {

	for (let i = 0; i < misile.length; i++) {
		canvas.beginPath()
		canvas.fillStyle = "#FF0000"
		canvas.arc(misile[i].pos[X], misile[i].pos[Y], 10, 0, 2*Math.PI)
		canvas.fill()
	}
/*
	canvas.beginPath()
	canvas.moveTo(pos[0] - 2.5, pos[1] -2.5)
	canvas.lineTo(objects[0].x - objects[0].size/2, objects[0].y - objects[0].size/2)
	canvas.stroke()

	// Objects
	canvas.beginPath()
	canvas.strokeStyle = "white"
	for (var i = 0; i < objects.length; i++) {
		canvas.save()
		canvas.translate(objects[i].x, objects[i].y)
		//canvas.rotate(objects[i].angle)
		canvas.rect(-objects[i].size, -objects[i].size, objects[i].size, objects[i].size)
		canvas.restore()
	}
	canvas.stroke()

	X = 150
	Y = 150
	ctx.save();
	ctx.beginPath();
	ctx.translate(X, Y);
	ctx.rotate(Math.PI)
	ctx.moveTo(-50, -50); // 150 150
	ctx.lineTo( 20, -40); // 220 160
	ctx.lineTo( 30, -20); // 230 180
	ctx.lineTo( 70, -10); // 270 190
	ctx.lineTo( 60,  40); // 260 240
	ctx.lineTo(-10,  50); // 190 250
	ctx.lineTo(-60,   0); // 140 200
	ctx.lineTo(-50, -50); // 150 150
	ctx.closePath();
	ctx.restore();
	ctx.stroke();
*/
}

function drawScore () {
	$('#demo').html(ship.angle + "  --  " + cosine(ship.angle) + "  --  " + sine(ship.angle) + "<br>" +
					ship.speed[X] + "  --  " + ship.speed[Y] + "<br>" +
					misile.length)
}