void Sumaf(){
    double num1, num2;

    printf("Pon el primer número a sumar: ");
    scanf(" %lf", &num1);
    printf("Pon el segundo número: ");
    scanf(" %lf", &num2);

    printf("%.2lf + %.2lf = %.2lf\n", num1, num2, num1+num2);
}

void Restaf(){
    double num1, num2;

    printf("Pon el primer número a restar: ");
    scanf(" %lf", &num1);
    printf("Pon el segundo número: ");
    scanf(" %lf", &num2);

    printf("%.2lf - %.2lf = %.2lf\n", num1, num2, num1-num2);
}

void Multif(){
    double num1, num2;

    printf("Pon el primer número a multiplicar: ");
    scanf(" %lf", &num1);
    printf("Pon el segundo número: ");
    scanf(" %lf", &num2);

    printf("%.2lf * %.2lf = %.3lf\n", num1, num2, num1*num2);
}

void Divif(){
    double num1, num2;

    printf("Pon el primer número a dividir: ");
    scanf(" %lf", &num1);
    printf("Pon el segundo número: ");
    scanf(" %lf", &num2);
    if (num2 == 0){
      printf("\nNo se puede dividir entre 0\n\n");
      Divif();
    }

    printf("%.2lf / %.2lf = %.3lf\n", num1, num2, num1/num2);
}

