#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "funcionesCalc.h"

int i=0;

void titulo(){
    system("clear");
    system("toilet -fpagga --gay \"== CALCULATOR3000 ==\"");
}

void switch1(){

    int operacion;
    char respuesta;

    printf("\t\n OPERACIONES:\n"
            "\t\t 1. Suma\n"
            "\t\t 2. Resta\n"
            "\t\t 3. Multiplicación\n"
            "\t\t 4. División\n\n"
            "\t ¿Cuál escojes?: ");
    scanf(" %d", &operacion);

    switch (operacion){
        case(1):
            printf("\nAs elegido la Suma\n\n");
            Sumaf();
            break;
        case(2):
            printf("\nAs elegido la Resta\n\n");
            Restaf();
            break;
        case(3):
            printf("\nAs elegido la Multiplicación\n\n");
            Multif();
            break;
        case(4):
            printf("\nAs elegido la División\n\n");
            Divif();
            break;
        default:
            printf("\n¿De verdad que no quieres nada? (Y/N): ");
            scanf(" %c", &respuesta);

            if(respuesta == 'Y' || respuesta == 'y'){
                printf("Adios\n");
                i++;
            }

            else{
                titulo();
                switch1();
            }
            break;
    }
}

void volver(){

    char respuesta;

    printf("\n¿Quieres hacer otra operación? (Y/N): ");
    scanf(" %c", &respuesta);

    if(respuesta == 'Y' || respuesta == 'y'){
        titulo();
        switch1();
    }
    else
        i++;
}

int main(){

    titulo();
    switch1();
    while (i<1){
        volver();
    }

    return EXIT_SUCCESS;
}


