// Global variables
const canvas = document.getElementById('board')
const width = canvas.width
const height = canvas.height
const ctx = canvas.getContext('2d')
const sizeCells = 25
var snake
var berry
var score
var vel
var end

// Classes
class Character {
	// Movimiento por todos los lados
	constructor (x, y) {
		this.tail = [[x, y]]
	}

	// Move the snake
	move () {

		// Move tail
		if (vel[0] != 0 || vel[1] != 0)
			for (let i = this.tail.length-1; i > 0; i--)
				this.moveTail(i)

		// Move the head
		this.tail[0][0] = this.tail[0][0] + vel[0]
		this.tail[0][1] = this.tail[0][1] + vel[1]

		// Check to see if the head hits the wall.
		wallHit(this.tail[0][0], this.tail[0][1])

		// Check if the head hits itself
		this.selfHit()
	}

	moveTail (index) {
		this.tail[index][0] = this.tail[index-1][0]
		this.tail[index][1] = this.tail[index-1][1]
	}

	selfHit () {
		// Checks if the head is hitting the tail
		if (this.tail.find(element => element != this.tail[0] && element[0] == this.tail[0][0] && element[1] == this.tail[0][1]))
			end = true
	}

	eat (food) {
		return (this.tail[0][0] === food.x && this.tail[0][1] === food.y)
	}

	pushTail () {
		// Create a new tail at the end of the tail
		this.tail.push([this.tail[this.tail.length-1][0], this.tail[this.tail.length-1][1]])
	}
}

class Food {
	constructor () {
		this.x = 0
		this.y = 0
		this.changePosition()
	}

	changePosition () {
		do {
			this.x = rand (20, 0)
			this.y = rand (20, 0)
		} while (isInSnake(snake.tail, this.x, this.y))
	}
}

// Functions
function rand (min, max) {
	// Ej. (2, 5) return --> 2 3 4
	return Math.floor (Math.random() * (max - min) + min)
}

function sumScore () {
	score++;
	document.getElementById('score').innerHTML = "YOU EAT " + score + " BERRIES."
}

function wallHit (x, y) {
	end = x == -1 || x == width / sizeCells || y == -1 || y == height / sizeCells
}

function isInSnake (arr, x, y) {
	if (arr.find(element => element[0] == x && element[1] == y))
		return true
}

function move () {

	// Check if the berry is eaten
	if (snake.eat (berry)) {
		berry.changePosition ()
		sumScore ()
		snake.pushTail()
	}

	// Move the snake
	snake.move()
}

function draw () {
	for (let i = 0; i < width; i += sizeCells) {
		for (let j = 0; j < height; j += sizeCells) {
			if (berry.x * sizeCells === i && berry.y * sizeCells === j)
				ctx.fillStyle = 'GREEN'
			else
				ctx.fillStyle = 'BLACK'

			for (let k = 0; k < snake.tail.length; k ++) {
				if (snake.tail[k][0] * sizeCells === i && snake.tail[k][1] * sizeCells === j)
					ctx.fillStyle = 'RED'
			}

			ctx.strokeStyle = 'WHITE'
			ctx.beginPath()
			ctx.rect (i, j, sizeCells, sizeCells)
			ctx.stroke()
			ctx.fill()
		}
	}
}

// Main Functions
function KeyPush (event) {

	switch (event.keyCode) {
		// Left
		case 37:
			if (vel[0] == 0 || snake.tail.length == 1)
				vel = [-1, 0]
			break
		// Up
		case 38:
			if (vel[1] == 0 || snake.tail.length == 1)
				vel = [0, -1]
			break
		// Right
		case 39:
			if (vel[0] == 0 || snake.tail.length == 1)
				vel = [1, 0]
			break
		// Down
		case 40:
			if (vel[1] == 0 || snake.tail.length == 1)
				vel = [0, 1]
			break
	}
}

function start () {
	snake = new Character (rand(20, 0), rand(20, 0))
	berry = new Food ()
	score = 0
	vel = [0, 0]
	end = false
	document.getElementById('score').innerHTML = "YOU EAT " + score + " BERRIES."
}

function game () {
	move ()
	if (end) {
		alert("Muerto")
		start()		
	}
	else
		draw ()
}

// Main Code
function main () {
	document.addEventListener("keydown", KeyPush)
	start ()
	setInterval(game, 100)
}