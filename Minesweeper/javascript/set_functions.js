function setMines() {

	// Set the mines
	// Coloca las minas

	var bien
	var positions = new Array

	if(cells_num[0] == undefined)
		return false
	
	do {
		bien = false
		x = Math.floor(Math.random() * DIM);
	   	y = Math.floor(Math.random() * DIM);

	   	for(var i = 0; i < positions.length && !bien; i++) {
			var a = positions[i].indexOf(x);
	  		var b = positions[i].lastIndexOf(y);
	 		if((a != -1 && a == 0) && (b != -1 && b == 1))
	 			bien = true
	 	}
	   	if (!bien) {
	   		positions.push([x,y])
	   		cells_num[x][y] = bomba
	   	}
	} while(positions.length != minas)

	setNumbers()
}

function setNumbers() {

	// Sets the numbers
	// Coloca los numeros

	for (var i = 0; i < cells_num.length; i++) {
		for (var j = 0; j < cells_num.length; j++) {
			num = 0
			if(cells_num[i][j] != bomba) {
				// Up
				setNumbers2( (i-1), j )
				// Down
				setNumbers2( (i+1), j )
				// Left
				setNumbers2(     i, (j-1) )
				// Right
				setNumbers2(     i, (j+1) )
				// U L
				setNumbers2( (i-1), (j-1) )
				// U R
				setNumbers2( (i-1), (j+1) )
				// D L
				setNumbers2( (i+1), (j-1) )
				// D R
				setNumbers2( (i+1), (j+1) )

			    cells_num[i][j] = num
			}
		}
	}
}

function setNumbers2(r, c) {

	if( (r >= 0  && r < DIM) && (c >= 0 && c < DIM) )
		if ( cells_num[r][c] == bomba )
			num++
}