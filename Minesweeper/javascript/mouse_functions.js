// Functions that change the background color of the cells when the mouse is above
// Funciones que cambian el color de fondo de las celdas cuando pasas el raton por encima

function selecciona(x) {
	x.style.backgroundColor = lightGray
}

function returnNormal(x) {
	x.style.backgroundColor = color[PosX][PosY]
}

function mouseEvent(x) {

	// The function that occurs when you press the mouse that draws the flag or numbers
	// La funcion que ocurre cuando presionas el raton que dibuja la bandera o los numeros

	if(!end) {
		
		if ( x.which == 1 ) {
			if(cells_flag[PosX][PosY] == false) {

				if(cells_num[PosX][PosY] == 9)
					dibujaMinas()

				else if(cells_num[PosX][PosY] == 0)
					dibujaNumbers(PosX, PosY)

				else {
					pintaNumero(PosX, PosY)
					cells_show[PosX][PosY] = true
				}
			}
		}
		else if ( x.which == 3 ) {

			if(!cells_flag[PosX][PosY] && !cells_show[PosX][PosY])
				pintaBandera()

			else if(cells_flag[PosX][PosY]) {
				celdas.innerHTML = ""
				cells_flag[PosX][PosY] = false
				cells_num[PosX][PosY] -= 10
			}

			checkSafe()
		}
	}
}