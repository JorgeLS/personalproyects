// Dimension of the board
var DIM
// Dimension of the grids
var dim
var minas = 10
var PosX
var PosY
var bomba = 9
var end = false
var num

var tablero
var filas
var celdas

var cells_num = undefined
var cells_flag
var cells_show
var color

var normalGray = "#AAAAAA"
var lightGray = "#CCCCCC"

function checkSafe() {

	// Check if the all flags are in the right position
	// Comprueba si se han puesta todas las banderas bien

	var countn = 0
	var countf = 0

	for (var i = 0; i < DIM; i++) {
		for (var j = 0; j < DIM; j++) {
			if(cells_num[i][j] == 19)
				countn++
			if(cells_flag[i][j] == true)
				countf++
		}
	}

	if(countn == minas && countn == countf) {
		end = true
		alert("Te has salvado")
	}
}

function creatBoard() {

	// Create the board
	// Crea el tablero

	tablero.innerHTML = ""

	for (var i = 0; i < DIM; i++) {

		tablero.innerHTML += "<div id=\"filas\" class=\"filas f"+ i +"\" " +
			"style=\"grid-template-columns:" +
			dim + "px " + dim + "px " + dim + "px " + dim + "px " +
			dim + "px " + dim + "px " + dim + "px " + dim + "px " +
			dim + "px " + dim + "px " + dim + "px " + dim + "px " +
			dim + "px " + dim + "px " + dim + "px " + dim + "px " +
			dim + "px " + dim + "px " + dim + "px " + dim + "px " +
			dim + "px " + dim + "px " + dim + "px " + dim + "px " +
			dim + "px " + dim + "px " + dim + "px " + dim + "px " +
			dim + "px " + dim + "px " + dim + "px " + dim + "px " +
			dim + "px " + dim + "px " + dim + "px " + dim + "px " +
			"\"></div>"
		filas = document.getElementsByClassName('filas')[i]
		
		for (var j = 0; j < DIM; j++) {
			filas.innerHTML += "<div id=\"columna\" class=\"columna f "+ i +" c "+ j +"\" " +
			"style=\" " +
				"width: "+ (dim-4) +"px; " +
				"height: "+ (dim-4) +"px; " +
				"font-size: "+ (dim/2.85) +"pt; " +
			"\" " +
			"onmouseover=\"getPos(this)\" " +
			"onmouseleave=\"returnNormal(this)\" " +
			"onmousedown=\"mouseEvent(event)\" ></div>"
		}
	}

	bzeroCells()
	setMines()
}

function bzeroCells() {

	// Sets all cells to the initial value
	// Pone todas las cells al valor inicial

	cells_num = new Array()
	cells_flag = new Array()
	cells_show = new Array()
	color = new Array()

	for (var i = 0; cells_num.length < DIM; i++) {
		cells_num[i] = new Array()
		cells_flag[i] = new Array()
		cells_show[i] = new Array()
		color[i] = new Array()
		for (var j = 0; j < DIM; j++) {
			cells_num[i].unshift(0)
			cells_flag[i].unshift(false)
			cells_show[i].unshift(false)
			color[i].unshift(normalGray)
		}
	}
}

function eventos() {

	// Create all the events to the buttons and the input
	// Crea los eventos para los botones y el input
	
	// Create a event to set the size
	// Crea un evento para coger el tamaño
	document.getElementsByClassName('bsize')[0].addEventListener("click", function(){ getSize(0) } )
	document.getElementsByClassName('bsize')[1].addEventListener("click", function(){ getSize(1) } )
	document.getElementsByClassName('bsize')[2].addEventListener("click", function(){ getSize(2) } )

	// Create a event to set the mines
	// Crea un evento para coger las minas
	document.getElementById('mines').addEventListener("change", getMines)

	// Create a event to start the game
	// Crea un evento que empieza el juego
	document.getElementById('start').addEventListener("click", reset)
	// Create a event to restart the game
	// Crea un evento que resetea el juego
	document.getElementById('restart').addEventListener("click", reset)
}

function reset() {

	// Reset or start the game
	// Resetea o empieza el juego

	creatBoard()
	document.getElementById('mines').value = minas
	end = false
}

function main() {

	// The main function
	// La función principal

	//alert("\t Hola!!, Vamos a jugar al Buscaminas  \n\n" +
	//	"Elige el tamaño del tablero y las minas a colocar")

	tablero = document.getElementById('tablero')
	document.getElementById('mines').value = minas

	eventos() 
}