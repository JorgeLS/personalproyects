function dibujaMinas() {

	// Draw the mines
	// Dibuja las minas

	for (var i = 0; i < cells_num.length; i++) {
		for (var j = 0; j < cells_num.length; j++) {
			var cell = document.getElementsByClassName("filas f"+ i)[0].getElementsByTagName('div')[j]
			if(cells_num[i][j] == bomba) {
				cell.innerHTML = "<img src=\"imagenes/mina.png\" " +
				"style=\"width: "+ (dim-dim/10) +"px; height: "+ (dim-dim/10) +"px;\" " +
				"draggable=\"false\" >"
			}
		}
	}
	end = true
}

function pintaNumero(fila, columna) {

	// Draw the numbers in the board
	// Draw the numbers in the board

	var celda = document.getElementsByClassName("filas f"+ fila)[0].getElementsByTagName('div')[columna]

	color[fila][columna] = lightGray
	celda.style.backgroundColor = color[fila][columna]

	if(cells_num[fila][columna] >= 10)
		cells_num[fila][columna] -= 10

	if(cells_num[fila][columna] != 0)
			celda.innerHTML = "<p " +/*
			"style=\"margin-top: "+ (dim/3.5 - 3) +"px;\" "+*/
			">"+ cells_num[fila][columna] +"</p>"
	else
		celda.innerHTML = ""
}

function pintaBandera() {

	// Draw the flags
	// Pinta las banderas

	cells_flag[PosX][PosY] = true

	cells_num[PosX][PosY] += 10

	celdas.innerHTML = "<img src=\"imagenes/bandera.png\" " +
		"style=\"width: "+ (dim-dim/10) +"px; height: "+ (dim-dim/10) +"px;\">"
}

function dibujaNumbers(fila, columna) {

	//  When the number of the cell is 0, start this function that create a recursive function to
	// draw the numbers  around the boxes that are 0
	//  Cuando el numero de la celda es 0, empieza esta funcion que crea un funcion recursiva
	// para pintar los numeros alrededor de las casillas que son 0

	pintaNumero(fila, columna)
	cells_show[fila][columna] = true

	// Up
	dibujaNumbers2( (fila-1), (columna) )
	// Down
	dibujaNumbers2( (fila+1), (columna) )
	// Left
	dibujaNumbers2( (fila), (columna-1) )
	// Right
	dibujaNumbers2( (fila), (columna+1) )
	// U L
	dibujaNumbers2( (fila-1), (columna-1) )
	// U R
	dibujaNumbers2( (fila-1), (columna+1) )
	// D L
	dibujaNumbers2( (fila+1), (columna-1) )
	// D R
	dibujaNumbers2( (fila+1), (columna+1) )
}

function dibujaNumbers2(r, c) {

	if( (r >= 0  && r < DIM) && (c >= 0 && c < DIM) ) {
		pintaNumero(r, c)
		if(cells_num[r][c] != 0)
			cells_show[r][c] = true
		if( cells_num[r][c] == 0 && !cells_show[r][c] )
			dibujaNumbers(r, c)
	}
}

