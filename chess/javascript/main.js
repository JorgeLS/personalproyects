var DIM = 75

var white = ["♙", "♖", "♘", "♗", "♕", "♔"]
var black = ["♟", "♜", "♞", "♝", "♛", "♚"]
// white:
// 	King ♔  Queen ♕  Alfil ♗  Hourse ♘  Tower ♖  Peon ♙
// black: 
//	King ♚  Queen ♛  Alfil ♝  Hourse ♞  Tower ♜  Peon ♟

var ficha
var row = 0
var column = 0

var muerte = [["white"],["black"]]

var lienzo
var final
var turno
var color

var num_turno = -1
var signo
var piece
var pos_piece
var end

var cells = new Array

function dibuja_tablero() {

	lienzo.clearRect(0, 0, 600, 600)

	lienzo.beginPath()
	for (var i=0; i < cells.length; i++)
		for (var j=0; j < cells.length; j++) {
			if (j % 2 == (i % 2))
				lienzo.fillStyle = "#FFFFFF"
			else
				lienzo.fillStyle = "#999999"
			lienzo.fillRect(0 + i*75, 0 + j*75, 75, 75)
		}
	lienzo.stroke()


	for(var i=0; i<cells.length; i++)
		for(var j=0; j<cells.length; j++){
			if(cells[i][j] == 10)
				cells[i][j] = 0
			if(cells[i][j] > 10)
				cells[i][j] -= 10
			if(cells[i][j] < -10)
				cells[i][j] += 10
		}

	dibuja_fichas()
}

function pon_fichas() {

	for (var i=0; i<cells.length; i++) {
		cells[1][i] = +1
		cells[6][i] = -1
	}
	for (var i=0; i<cells.length; i++) {
		if( i < 3 ) {
			cells[0][i] = +2 + i
			cells[7][i] = -2 - i
		}
		else if( i > 4) {
			cells[0][i] = +9 - i
			cells[7][i] = -9 + i
		}
		else {
			cells[0][i] = +2 + i
			cells[7][i] = -2 - i
		}
	}
}

function dibuja_fichas() {

	comprueba_cambio_peon()

	lienzo.beginPath()
	lienzo.fillStyle = "#000000"
	lienzo.font = "80px Arial"
	for(var i=0; i<cells.length; i++)
		for (var j=0; j<cells.length; j++) {
			if( cells[i][j] == 10 ) continue
			if( navigator.appVersion.indexOf('Win') == -1 ) {
				// Linux
				if(Math.sign( cells[i][j] ) == 1)
					lienzo.fillText(black[+cells[i][j]-1], 5+DIM*j, 62+DIM*i)
				else if(Math.sign(cells[i][j]) == -1)
					lienzo.fillText(white[-cells[i][j]-1], 5+DIM*j, 62+DIM*i)
			}
			else {
				// Windows
				if(Math.sign( cells[i][j] ) == 1)
					lienzo.fillText(black[+cells[i][j]-1], DIM*j - 2, DIM*i + 67)
				else if(Math.sign(cells[i][j]) == -1)
					lienzo.fillText(white[-cells[i][j]-1], DIM*j - 2, DIM*i + 67)
			}
		}
	lienzo.stroke()
}

function get_coord() {

  	var x = event.offsetX;
	var y = event.offsetY;

  	for(var i=0; i<cells.length; i++)
  		for(var j=0; j<cells.length; j++)
  			if ( (x > i*DIM && x < DIM + i*DIM) && (y > j*DIM && y < DIM + j*DIM) ) {
  				row = i
  				column = j
  			}
  	// Para debugger y comprovacion
  	/*
  	demo.innerHTML = ""
  	for(var i=0; i<cells.length; i++){
  		for(var j=0; j<cells.length; j++)
  			demo.innerHTML += cells[i][j] + " "
  		demo.innerHTML += "<br>"
  	}
  	*/
  	if(!end)
  		move()
}

function move() {


  	if (cells[column][row] == 10) {
  		move_piece()  		
  		num_turno -= Math.sign(num_turno) * 2
  	}
  	else if (cells[column][row] > 10 || cells[column][row] < -10) {
  		come_ficha()
  		num_turno -= Math.sign(num_turno) * 2
  	}
  	else {
		piece = cells[column][row]
		pos_piece = [row, column]
  		selecciona_ficha()
  	}

	if (num_turno == -1)
  		turno.innerHTML = "TURNO DE LAS BLANCAS"
  	else
  		turno.innerHTML = "TURNO DE LAS NEGRAS"

  	if( end = comprueba_rey() ) {
  		if(num_turno == -1)
  			final.innerHTML = "HAN GANADO LAS NEGRAS"
  		else
  			final.innerHTML = "HAN GANADO LAS BLANCAS"
  	}
}

function move_piece() {

	cells[column][row] = piece
	cells[pos_piece[1]][pos_piece[0]] = 0
	
	dibuja_tablero()
}

function come_ficha() {

	if(num_turno == -1)
		muerte[1].push( black[(cells[column][row]) - 11] )

	if(num_turno == 1)
		muerte[0].push( white[-(cells[column][row]) - 11] )

	cells[column][row] = piece
	cells[pos_piece[1]][pos_piece[0]] = 0

	dibuja_tablero()

	dead_white.innerHTML = "- " + muerte[0]
	dead_black.innerHTML = "- " + muerte[1]
}

function comprueba_rey() {

	for(var i=0; i<cells.length; i++) {
		for (var j=0; j<cells.length; j++)
			if(cells[i][j] == num_turno * 6)
				return end = false
	}	
	return true
}

function comprueba_cambio_peon() {

	if( cells[column][row] == -1 && column == 0 )
		cells[column][row] = -5

	else if( cells[column][row] == 1 && column == 7 )
		cells[column][row] = 5
}

function movement_color() {

	color = document.getElementById("color").checked

	dibuja_tablero()
}

function reset() {

	final.innerHTML = ""
	turno.innerHTML = "TURNO DE LAS BLANCAS"
	dead_white.innerHTML = "-"
	dead_black.innerHTML = "-"
	num_turno = -1
	end = false
	muerte = [[],[]]
	for(var i=0; i<cells.length; i++)
		for (var j=0; j<cells.length; j++)
			cells[i][j] = 0
	pon_fichas()
	dibuja_tablero()
}

function main() {

	lienzo = document.getElementById("tablero").getContext("2d")
	turno = document.getElementById("turno")
	final = document.getElementById("final")
	dead_white = document.getElementById("white")
	dead_black = document.getElementById("black")

	// Inicialitating cells 
	for (var i = 0; cells.length < 8; i++) {
		cells[i] = new Array()
		for (var j = 0; j < 8; j++) {
			cells[i].unshift(0)
		}
	}

	var demo = document.getElementById('demo')

	movement_color()
	reset()

	// Botton of reset
	document.getElementById("btn_reset").addEventListener("click", reset)
	// Input
	document.getElementById("color").addEventListener("change", movement_color)
	// get coordenadas
	document.getElementById("tablero").addEventListener("click", get_coord)
}