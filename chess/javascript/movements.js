function selecciona_ficha() {

	dibuja_tablero()
	ficha = 0

	lienzo.beginPath()
	lienzo.lineWidth = "4"
	if (cells[column][row] != 0) {
		lienzo.strokeStyle = "#00FF00"
		lienzo.strokeRect(row*DIM, column*DIM, DIM, DIM)

		ficha = cells[column][row]
	}
	
	if (ficha != 0 && Math.sign(ficha) == num_turno) {
		lienzo.fillStyle = "#0055AA"
		lienzo.strokeStyle = "#FF0000"
		switch (ficha) {
			//PEON
			case 1: case -1:
				if ( (row >= 0 && row < cells.length) &&
					(column + ficha*2 >= 0 && column + ficha*2 < cells.length) &&
					(column == 1 || column == 6) )
					movimiento_peon (row, column, ficha, 3)
				else
					movimiento_peon (row, column, ficha, 2)

				dibuja_comer_peon(row + 1, column + ficha, ficha)
				dibuja_comer_peon(row - 1, column + ficha, ficha)

				break

			//TORRE
			case 2: case -2:
				// Horizintal
				// izquierda
				for (var i = row-1; i >= 0 && movimiento_ficha (i, column); i--) {}
				// derecha
				for (var i = row+1; i < DIM && movimiento_ficha (i, column); i++) {}

				// Vertical
				// up
				for (var i = column-1; i >= 0 && movimiento_ficha (row, i); i--) {}
				// down
				for (var i = column+1; i < DIM && movimiento_ficha (row, i); i++) {}
				break
			
			// CABALLO
			case 3: case -3:
				var sign1
				var sign2
				for (var i = 1; i < 3; i++) {
					// Para que cambie de 1 a 2 la columna
					if ( i == 1 )
						sign1 = 1
					else
						sign1 = -1
					for (var j = 0; j < 4; j++) {
						// Cambia de signo de 1 || 2 a -1 || -2 la fila
						if ( j % 2 == 0)
							sign2 = 1
						else
							sign2 = -1
						// Abajo
						if (j < 2)
							movimiento_ficha ( row+( (i+sign1) * sign2 ), (column+i) )
						// Arriba	
						if (j >= 2)
							movimiento_ficha ( row+( (i+sign1) * sign2 ), (column-i) )
					}
				}
				break

			// ALFIL
			case 4: case -4:
				// Descendete
				for (var i = 1; i < DIM && movimiento_ficha(row-i, column-i); i++) {}
				for (var i = 1; i < DIM && movimiento_ficha(row+i, column+i); i++) {}
				// Ascendente
				for (var i = 1; i < DIM && movimiento_ficha(row-i, column+i); i++) {}
				for (var i = 1; i < DIM && movimiento_ficha(row+i, column-i); i++) {}
				break

			// REINA
			case 5: case -5:
				// FORMA ALFIL
				// Descendete
				for (var i = 1; i < DIM && movimiento_ficha(row-i, column-i); i++) {}
				for (var i = 1; i < DIM && movimiento_ficha(row+i, column+i); i++) {}
				// Ascendente
				for (var i = 1; i < DIM && movimiento_ficha(row-i, column+i); i++) {}
				for (var i = 1; i < DIM && movimiento_ficha(row+i, column-i); i++) {}

				// FORMA TORRE
				// Horizintal
				// izquierda
				for (var i = row-1; i >= 0 && movimiento_ficha (i, column); i--) {}
				// derecha
				for (var i = row+1; i < DIM && movimiento_ficha (i, column); i++) {}
				// Vertical
				// up
				for (var i = column-1; i >= 0 && movimiento_ficha (row, i); i--) {}
				// down
				for (var i = column+1; i < DIM && movimiento_ficha (row, i); i++) {}
				break

			// REY
			case 6: case -6:
				// FORMA TORRE
				// Horizintal
				// izquierda
				movimiento_ficha (row+1, column)
				// derecha
				movimiento_ficha (row-1, column)
				// Vertical
				// up
				movimiento_ficha (row, column+1)
				// down
				movimiento_ficha (row, column-1)

				// FORMA ALFIL
				// Descendete
				movimiento_ficha (row+1, column+1)
				movimiento_ficha (row-1, column-1)
				// Ascendente
				movimiento_ficha (row-1, column+1)
				movimiento_ficha (row+1, column-1)
				break
			}
		}
	lienzo.stroke()
}

function movimiento_ficha(r, c) {
	
	lienzo.beginPath()
	
	lienzo.lineWidth = "4"
	lienzo.fillStyle = "#0055AA"
	lienzo.strokeStyle = "#FF0000"

	if ( ( r >= 0 && r < cells.length )
		&& ( c >= 0 && c < cells.length ) ) {
		if (cells[c][r] == 0) {
			if (color)
				lienzo.fillRect(DIM * r, DIM * c, DIM, DIM)
			cells[c][r] = 10
			return true
		}
		else if (Math.sign(cells[c][r]) != Math.sign(ficha)) {
			if (color)
				lienzo.strokeRect(DIM * r, DIM * c, DIM, DIM)
			cells[c][r] += 10 * -num_turno
			return false
		}
		else
			return false
	}
	else
		return true
}

function movimiento_peon (r, c, f, v) {
	for (var i = 1; i < v; i++) {
		if ( cells[c + f*i][r] == 0 ) {
			if (color)
				lienzo.fillRect(DIM * r, DIM * (c + f*i), DIM, DIM)
			cells[c + f*i][r] = 10
		}
		else
			break
	}
}

function dibuja_comer_peon(r, c, f) {
	
	lienzo.beginPath()
	
	lienzo.lineWidth = "4"
	lienzo.fillStyle = "#0055AA"
	lienzo.strokeStyle = "#FF0000"

	if ( cells[c][r] != 0 &&
		r >= 0 && r < cells.length &&
		c >= 0 && c < cells.length ) {
		if ( Math.sign(cells[c][r]) != Math.sign(f) ) {
			if (color)
				lienzo.strokeRect(DIM * r, DIM * c, DIM, DIM)
			cells[c][r] += 10 * -num_turno
		}
	}
	lienzo.stroke()
}