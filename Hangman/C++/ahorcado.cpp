#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LENGTH 0x100
#define NUM 10

char letra_introducida[NUM];

void titulo(){

    system("clear");
    system("toilet -fpagga -Fcrop:border:metal '   AHORCADO   '");
    printf("\n\n");
}

void dibuja (char *word) {

    int cont = 0;

    // Muestra la palabra a trozos hasta que vayas acertando letras
    printf("Palabra a Adivinar: \t");
    while(word[cont] != '\0'){
        printf("%c ", word[cont]);
        cont++;
    }
    printf("\n");
}

void letra_fallada ( bool comprobacion ) {

    // Muestra las palabras falladas
    if( !comprobacion )
        printf("\nHas fallado la letra");

    printf("\nPalabras Falladas: ");
    for(int i=0; i<NUM; i++)
        printf("%c ", letra_introducida[i]);

    printf("\n\n");
}

void dime_palabra ( char *palabra ) {

    printf("Dime la palabra a adivinar: ");
    scanf(" %[^\n]", palabra);

    // Comprueba que la palabra sea en minúscula y que pueda haber espacios
    for(int i=0; i < strlen(palabra); i++)
        if( (palabra[i] < 97 && palabra[i] != 32) || (palabra[i] > 122 && palabra[i] != 32) ){
            printf("\nEsa palabra no me vale, pon otra.\n\n");
            dime_palabra(palabra);
        }
}

char coje_letra () {

    char letra = ' ';
    bool comp = false;

    printf("Dime la letra que vas a escojer: ");
    scanf(" %1c", &letra);

    // Comprueba que esa letra no esté puesta y sea una letra minúscula
    for(int i=0; i<NUM; i++)
        if( letra == letra_introducida[i] )
            comp = true;

    if(!comp){
        if(letra > 96 && letra < 123)
            return letra;
        else{
            printf("Solo puedes introducir letras minúsculas\n");
            coje_letra();
        }
    }
    else{
        printf("Esa letra ya la has puesto\n");
        coje_letra();
    }
}

bool comprueba_letra ( char *word1, char *word2, int turno, char letra ) {

    bool comp = false;

    // Comprueba letra a letra si es igual que la palabra a adivinar
    for(int i=0; i<strlen(word2); i++){
        if(word1[i] == letra){
            word2[i] = word1[i];
            comp = true;
        }
    }

    if(!comp)
        letra_introducida[turno] = letra;
    return comp;
}

int main (int argc, char *argv[]) {

    // Datos de la Palabra a adivinar

    char wordA[LENGTH]; //Palabra a adivinar
    char wordR[LENGTH]; //Palabra de respuesta

    bool comp = false;

    dime_palabra (wordA);
    strcpy(wordR, wordA);

    // Cambia la palabra a adivinar a _
    for(int i=0; i<LENGTH; i++){
        if(wordR[i] == '\0') break;
        if(wordR[i] != ' ')
            wordR[i] = '_';
    }

    // Inicialización de Datos

    char letra;
    bool end = false, comprueba = true;
    int intentos = 0;

    // Da a letra_introducida el valor space
    for(int i=0; i<NUM; i++)
        letra_introducida[i] = ' ';

    do{
        // Dibuja el titulo
        titulo();

        // Dibuja la palabra a adivinar y las letras falladas
        dibuja(wordR);
        letra_fallada( comprueba );

        // Comprueba la letra inctroducida
        comprueba = comprueba_letra ( wordA, wordR, intentos, coje_letra() );

        // Compara la palabra a adivinar y las letras introducidas
        if( strcmp(wordA, wordR) == 0)
            end = true;

        // Comprueba los intentos del jugador y añade intentos si falla letra
        if( intentos > 6 )
            end = true;
        else if( !comprueba )
            intentos++;

    }while(end == false);

    titulo();
    dibuja(wordR);

    // Comprueba el ganador
    if( intentos > 6 )
        printf("\n\n\tHAS PERDIDO QUE MAL :(\n\n");
    else
        printf("\n\n\tHAS GANADO BIEEEN :)\n\n");

    return EXIT_SUCCESS;
}


