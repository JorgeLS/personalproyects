var width_tablero = 600
var height_tablero = 600
var width_palabra = 350
var height_palabra = 200

var lienzo
var palabra

var turno = 6
var PosYw = 120
var end = false

var palabra_introducida
var palabra_adivinar
var letra
var letras_adivinadas = 0
var letra_fallada
var letras_puestas = [0]

function dibuja() {

    lienzo.clearRect(0, 0, 600, 600)
    fondo()

    if( end == true && turno != 0 ){
        // Cabeza
        lienzo.beginPath()
        lienzo.fillStyle = "#000000"
        lienzo.arc(300, 155, 51, 0, 2 * Math.PI)
        lienzo.fill()
        lienzo.beginPath()
        lienzo.fillStyle = "#FFCC00"
        lienzo.arc(300, 155, 49, 0, 2 * Math.PI)
        lienzo.fill()

        //Ojos
        lienzo.beginPath()
        lienzo.fillStyle = "#000000"
        lienzo.arc(280, 145, 8, 0, 2 * Math.PI)
        lienzo.arc(320, 145, 8, 0, 2 * Math.PI)
        lienzo.fill()

        //Boca
        lienzo.beginPath()
        lienzo.strokeStyle = "#000000"
        lienzo.lineWidth = 4;
        lienzo.lineJoin = "round";
        lienzo.lineCap = "round";
        lienzo.moveTo(280, 175);
        lienzo.lineTo(295, 185);
        lienzo.lineTo(305, 185)
        lienzo.lineTo(320, 175);
        lienzo.stroke()

        lienzo.beginPath()
        lienzo.strokeStyle = "#000000"
        lienzo.lineWidth = 3
        //Cuerpo
        lienzo.moveTo(300, 205)
        lienzo.lineTo(300, 350)

        //Piernas
        lienzo.moveTo(300, 350)
        lienzo.lineTo(220, 470)
        lienzo.moveTo(300, 350)
        lienzo.lineTo(380, 470)

        //Brazos
        lienzo.moveTo(300, 220)
        lienzo.lineTo(220, 300)
        lienzo.moveTo(300, 220)
        lienzo.lineTo(380, 300)
        lienzo.stroke()
    }
    else{
        if(turno < 6){
            // Cabeza
            lienzo.beginPath()
            lienzo.fillStyle = "#000000"
            lienzo.arc(275, 120, 51, 0, 2 * Math.PI)
            lienzo.fill()
            lienzo.beginPath()
            lienzo.fillStyle = "#FFCC00"
            lienzo.arc(275, 120, 49, 0, 2 * Math.PI)
            lienzo.fill()

            //Ojos
            lienzo.beginPath()
            lienzo.fillStyle = "#000000"
            lienzo.font = "20px TimesNewBrowman"
            lienzo.fillText("X", 240, 120)
            lienzo.fillText("X", 270, 120)
            lienzo.fill()

            //Boca
            lienzo.beginPath()
            lienzo.strokeStyle = "#000000"
            lienzo.lineWidth = 4;
            lienzo.lineJoin = "round";
            lienzo.lineCap = "round";
            lienzo.moveTo(240, 145);
            lienzo.lineTo(256, 135);
            lienzo.lineTo(274, 135)
            lienzo.lineTo(290, 145);
            lienzo.stroke()

            lienzo.beginPath()
            lienzo.strokeStyle = "#000000"
            lienzo.lineWidth = 3
            if(turno < 5){
                // Cuerpo
                lienzo.moveTo(300, 165)
                lienzo.lineTo(300, 350)
                if(turno < 4){
                    // Piernas
                    lienzo.moveTo(300, 350)
                    lienzo.lineTo(400, 450)
                    if(turno < 3){
                        lienzo.moveTo(300, 350)
                        lienzo.lineTo(200, 450)
                        if(turno < 2){
                            // Brazos
                            lienzo.moveTo(300, 200)
                            lienzo.lineTo(400, 250)
                            if(turno < 1){
                                lienzo.moveTo(300, 200)
                                lienzo.lineTo(200, 250)
                            }
                        }
                    }
                }
            }
        }
        lienzo.stroke()
    }
}

function fondo() {

    //Color de Fondo
    lienzo.beginPath()
    lienzo.lineWidth = 2
    lienzo.fillStyle = "#66CCFF"
    lienzo.fillRect(0, 0, width_tablero, height_tablero)
    lienzo.fillStyle = "#996633"
    lienzo.strokeStyle = "#000000"
    lienzo.fillRect(0, 470, width_tablero, height_tablero)
    lienzo.strokeRect(0, 470, width_tablero, height_tablero)
    lienzo.stroke()

    //Complementos del fondo
    lienzo.beginPath()
    lienzo.lineWidth = 2
    lienzo.strokeStyle = "#444444"
    for(var i = 0; i<4; i++){
        lienzo.moveTo(0, 502.5 + 32.5 * i)
        lienzo.lineTo(width_tablero,  502.5 + 32.5 * i)
    }
    lienzo.stroke()

    if( end != true || ( end == true && turno == 0 ) ){
        //Cuerda
        lienzo.beginPath()
        lienzo.lineWidth = 15
        lienzo.strokeStyle = "#000000"
        lienzo.lineJoin = "square"
        lienzo.moveTo(600, 20)
        lienzo.lineTo(300, 20)
        lienzo.lineTo(300, 160)
        lienzo.stroke()
    }
}

function clear() {

    //Hace un Reset de todo
    lienzo.clearRect(0, 0, width_tablero, height_tablero)
    palabra.clearRect(0, 0, width_palabra, height_palabra)
    end = false
    turno = 6
    dibuja()
    letras_adivinadas = 0
    letras_puestas = [0]
    document.getElementById("fallos").innerHTML = ""
}

function rayas(longitud) {

    var num1 = (longitud - 1) * 10
    var num2 = (310 - num1) / longitud
    var num3 = 20

    palabra.beginPath()
    palabra.lineWidth = 2
    palabra.moveTo(num3, PosYw)
    for(var i=1; i<longitud; i++){
        num3 = num3 + num2
        palabra.lineTo(num3, PosYw)
        num3 = num3 + 10
        palabra.moveTo(num3, PosYw)
    }
    palabra.lineTo(330, PosYw)
    palabra.stroke()
}

function wordF(pos) {

    var num1 = (palabra_adivinar.length - 1) * 10
    var num2 = (310 - num1) / palabra_adivinar.length
    var num3 = 95 / Math.pow(palabra_adivinar.length, .5)

    //Pone las rayas a la letras
    rayas(palabra_adivinar.length)

    //Dibuja las letras en las casillas correspondientes
    palabra.beginPath()
    palabra.font = 25 - palabra_adivinar.length * 1.5 + "pt Arial"
    for(var i=0; i<palabra_adivinar.length; i++){
        if(pos == i)
            palabra.fillText(palabra_adivinar[i], num3, PosYw-10)
        num3 = num3 + num2 + num1/palabra_adivinar.length
    }
    palabra.stroke()
}

function ponerP() {

    palabra_introducida = document.getElementById("palabra_input").value
    document.getElementById("comprobacion").innerHTML = ""
    document.getElementById("palabra_input").value = ""
    //Comprueba que lo que se ha introducido es una palabra
    if(palabra_introducida != undefined){
        var palabra_bien = true
        for(var i = 0; i<10; i++)
            if(palabra_introducida.includes(i))
                palabra_bien = false
        if(palabra_bien){
            clear()

            palabra_adivinar = palabra_introducida
            rayas(palabra_introducida.length)
        }
        else
            document.getElementById("comprobacion").innerHTML = "Debes poner una palabra solo con letras"
    }
}

function ponerL() {

    if(!end){
        //Llama a la palabra
        letra = document.getElementById("letra_input").value[0]
        document.getElementById("comprobacion").innerHTML = ""
        document.getElementById("letra_input").value = ""
        //Comprueba que lo que se ha introducido es una letra
        if(letra != undefined &&
            letra != 0 && letra != 1 && letra != 2 && letra != 3 && letra != 4 &&
            letra != 5 && letra != 6 && letra != 7 && letra != 8 && letra != 9 ){
            if( !letras_puestas.includes(letra) ){
                if( palabra_introducida.includes(letra)){
                    //Compueba donde está la letra introducida
                    var n = 0
                    do{
                        n = palabra_introducida.indexOf(letra, n)
                        if(n != -1){
                            wordF(n)
                            letras_adivinadas++
                        }
                        n++;
                    }while(n > 0)
                        letras_puestas.unshift(letra)
                }
                else{
                    document.getElementById("comprobacion").innerHTML = "No esta esa letra"
                    letra_fallada.insertAdjacentHTML("beforeend", letra + " ")
                    letras_puestas.unshift(letra)
                    turno--
                }
            }
            else
                document.getElementById("comprobacion").innerHTML = "Esa letra ya la has puesto"
        }
        else
            document.getElementById("comprobacion").innerHTML = "Debes poner una letra"
    }
    //Dibuja el tablero nuevo y comprueba
    dibuja()
    compreba()

    if(end == true)
        dibuja()
}

function compreba() {

    //Comprueba si se ha acabado los turnos
    if(turno == 0){
        document.getElementById("comprobacion").innerHTML = "Has PERDIDO, HAS MUERTO"
        end = true
    }
    //Comprueba si se ha acertado la palabra
    if(letras_adivinadas == palabra_introducida.length){
        document.getElementById("comprobacion").innerHTML = "Has GANADO te has salvado"
        end = true
    }
}

function main() {

    lienzo = document.getElementById("lienzo").getContext("2d")
    palabra = document.getElementById("word").getContext("2d")
    letra_fallada = document.getElementById("fallos")
    document.getElementById("palabra_input").value = ""

    lienzo.clearRect(0, 0, width_tablero, height_tablero)

    dibuja()
}
