#include <stdio.h>
#include <stdlib.h>

#define DIMF 8
#define DIMC 8
#define TPoint 15

/* Define variables gobales */
const char *simbolo[] = {" ", "O", "S"};
int pos[2], points[] = {0, 0};

void title(){
    //Dibuja el titulo
    system("clear");
    printf("\n\n");
    system("toilet -fpagga -F crop 'Juego Del Oso'");
    printf("\n  Puntos:\n   JUGADOR 1: %i \t JUGADOR 2: %i\n\n", points[0], points[1]);
}

void dibujo(){
    //Dibuja las lineas del tablero
    printf("\t");
    for(int j=0; j<DIMC; j++)
        printf("+---");
    printf("+\n");
}

void tablero(char T[DIMF][DIMC], int turn){

    //Dibuja tablero
    title();

    //Dibuja los numero de las columnas
    printf("\t");
    for(int j=0; j<DIMC; j++)
        printf("  %i ", j);
    printf("\n");

    for(int i=0; i<DIMF; i++){
        dibujo();
        printf("     %i\t|", i);
        for(int j=0; j<DIMC; j++)
            printf(" %c |", T[i][j]);
        printf("\n");
    }
    dibujo();
    printf("\n");
}

void pregunta(char T[DIMF][DIMC], int t){

    char letra;

    if(t % 2 == 0)
        printf("  TURNO DEL JUGADOR 1\n\n");
    else
        printf("  TURNO DEL JUGADOR 2\n\n");


    //Pregunta posicion
    printf(" Dime donde quieres poner la letra: ");
    scanf(" %i %i", &pos[0], &pos[1]);

    if(T[pos[0]][pos[1]] != *simbolo[0]){
        tablero(T, t);
        printf(" La posicion esta ya escojida.\n");
        pregunta(T, t);
    }

    //Pregunta letra
    printf("Dime la letra: ");
    scanf(" %c", &letra);

    if(letra == 'o' || letra == 'O')
        T[pos[0]][pos[1]] = *simbolo[1];
    else if(letra == 's' || letra == 'S')
        T[pos[0]][pos[1]] = *simbolo[2];
    else{
        tablero(T, t);
        printf(" No vale esa letra.\n");
        pregunta(T, t);
    }
}

bool comprueba(char T[DIMF][DIMC], int *turn){

    bool changeTurn = true;

    //Cuando es S la letra
    if(T[pos[0]][pos[1]] == *simbolo[2] &&
            T[pos[0]][pos[1]-1] == *simbolo[1] &&
            T[pos[0]][pos[1]+1] == *simbolo[1]){
        points[*turn % 2]++;
        changeTurn = false;
    }
    if(T[pos[0]][pos[1]] == *simbolo[2] &&
            T[pos[0]-1][pos[1]] == *simbolo[1] &&
            T[pos[0]+1][pos[1]] == *simbolo[1]){
        points[*turn % 2]++;
        changeTurn = false;
    }

    //Cuando es O la letra
    if(pos[0] <= 5){
        if(T[pos[0]][pos[1]] == *simbolo[1] &&
                T[pos[0]+1][pos[1]] == *simbolo[2] &&
                T[pos[0]+2][pos[1]] == *simbolo[1]){
            points[*turn % 2]++;
            changeTurn = false;
        }
    }
    if(pos[0] >= 2){
        if(T[pos[0]][pos[1]] == *simbolo[1] &&
                T[pos[0]-1][pos[1]] == *simbolo[2] &&
                T[pos[0]-2][pos[1]] == *simbolo[1]){
            points[*turn % 2]++;
            changeTurn = false;
        }
    }
    if(pos[1] <= 5){
        if(T[pos[0]][pos[1]] == *simbolo[1] &&
                T[pos[0]][pos[1]+1] == *simbolo[2] &&
                T[pos[0]][pos[1]+2] == *simbolo[1]){
            points[*turn % 2]++;
            changeTurn = false;
        }
    }
    if(pos[1] >= 2){
        if(T[pos[0]][pos[1]] == *simbolo[1] &&
                T[pos[0]][pos[1]-1] == *simbolo[2] &&
                T[pos[0]][pos[1]-2] == *simbolo[1]){
            points[*turn % 2]++;
            changeTurn = false;
        }
    }

    if(changeTurn == true)
        *turn = *turn + 1;
}

int main(){

    char celdas[DIMF][DIMC];
    bool end = false;
    int turno = 0;

    for(int i=0; i<DIMF; i++)
        for(int j=0; j<DIMC ; j++)
            celdas[i][j] = *simbolo[0];

    do{
        tablero(celdas, turno);
        pregunta(celdas, turno);

        comprueba(celdas, &turno);

        if( points[0] >= TPoint )
            end = true;
        else if( points[1] >= TPoint)
            end = true;

    }while(end == false);

    tablero(celdas, turno);

    if(turno % 2 == 0)
        system("toilet -fpagga -F metal 'El JUGADOR 1 HA GANADO'");
    else
        system("toilet -fpagga -F metal 'El JUGADOR 2 HA GANADO'");

    printf("\n\n");

    return EXIT_SUCCESS;
}


