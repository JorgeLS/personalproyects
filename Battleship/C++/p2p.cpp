#include <stdio.h>
#include <stdlib.h>

#define numB 3

struct Points{

    int x;
    int y;
};

void barcox(Points num, Points barco[numB]){

    for(int i=0; i<numB; i++)
        if(num.x == barco[i].x)
            printf(" %i\n", i+1);
}

void barcoy(Points num, Points barco[numB]){

    for(int i=0; i<numB; i++)
        if(num.y == barco[i].y)
            printf(" %i\n", i+1);
}

bool encontrado(Points num1, Points num2, Points barco[numB]){

    if( num1.x == num2.x && num1.y == num2.y){
        printf("Has dado al barco");
        barcox(num1, barco);
        return true;
    }
    else if(num1.x == num2.x){
        printf("Has acertado la x del barco");
        barcox(num1, barco);
        return false;
    }
    else if(num1.y == num2.y){
        printf("Has acertado la y del barco");
        barcoy(num1, barco);
        return false;
    }
    else
        return false;
}

Points movimiento(Points num){

    printf("¿A donde vas a disparar?: ");
    scanf(" %i %i", &num.x, &num.y);

    if(num.x > 20 || num.x < 0 || num.y > 20 || num.y < 0){
        printf("Por allí no estaban, apunta mejor entre [0-20]\n\n");
        movimiento(num);
    }
    else
        return num;
}

int main(){

    Points barco[numB] = {
        { 10, 15 },
        { 0 , 17 },
        { 20, 0  }};
    Points buscador;

    bool p2p[numB] = {false, false, false};
/*
    for(int i=0; i<numB; i++)
        printf(" %i %i\n", barco[i].x, barco[i].y);
*/
    printf("\n\tDispara para dar a los Barcos\n\n"
            "Nos han dicho que estan en x[0-20] y y[0-20]\n\n");
    do{
        buscador = movimiento(buscador);

        for(int i=0; i<numB; i++)
            if(p2p[i] == false)
                p2p[i] = encontrado(barco[i], buscador, barco);

        printf("\n");

    }while(p2p[0] == false || p2p[1] == false || p2p[2] == false);


    printf("\tMuy bien as destruido los barcos\n"
            "Barco 1: %2ix %2iy\n"
            "Barco 2: %2ix %2iy\n"
            "Barco 3: %2ix %2iy\n",
            barco[0].x, barco[0].y,
            barco[1].x, barco[1].y,
            barco[2].x, barco[2].y);

    return EXIT_SUCCESS;
}


