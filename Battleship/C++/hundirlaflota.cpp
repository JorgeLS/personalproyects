#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 7

int pos[2];
const char *simbolo[] = {"🌊", "💥"};

void title(){

    system("clear");
    system("toilet -fpagga -Fcrop 'HUNDIR LA FLOTA'");
}

void dibuja(int x){
    printf("\t");
    if(x == 0){
        for(int i=0; i<DIM; i++)
            printf("―――――");
        printf("―\n");
    }
    else{
        for(int i=0; i<DIM; i++)
            printf("|――――");
        printf("|\n");
    }
}
void imprimeTablero(char M[DIM][DIM]){

    //title();
    printf("\t");
    for(int i=0; i<DIM; i++)
        printf("  %i  ", i);
    printf("\n");
    for(int i=0; i<DIM; i++){
        dibuja(i);
        printf("    %i\t|", i);
        for(int j=0; j<DIM; j++)
            printf("  %c |", M[i][j]);//M[i][j]);
        printf("\n");
    }
    dibuja(0);
}

int relleno(char M[DIM][DIM]){

    for(int f=0; f<DIM; f++)
        for(int c=0; c<DIM; c++)
            M[f][c] = *simbolo[1];
}

void ponBarcos(char *T[DIM][DIM]){

    bool endR = false;
    int direccion = 0, longBarcos = 5;

    while(endR == false){

        bool noChoque = true; //Condicion de que no choque
        int X = rand() % DIM;
        int Y = rand() % DIM;

        //Primer Barco(Portaaviones)
        if(direccion == 0){ //Esto eligue que Barco cojer

            if(X <= 2){
                for(int i=0; i<longBarcos; i++)
                    *T[X+i][Y] = *simbolo[1]; //Vertical
                direccion++;
                longBarcos--;
            }
        }

        //Segundo Barco(Acorazado)
        else if(direccion == 1){

            if(Y <= 3){
                for(int j=0; j<longBarcos; j++)
                    if(*T[X][Y+j] != *simbolo[0]) //Comprueba si no choca
                        noChoque = false; //Si choca no deja continuar

                if(noChoque == true){
                    for(int i=0; i<longBarcos; i++)
                        *T[X][Y+i] = *simbolo[1]; //Horizontal
                    direccion++;
                    longBarcos--;
                }
            }
        }

        //Tercer Barco(Crucero)
        else if(direccion == 2){

            if(X <= 4){
                for(int j=0; j<longBarcos; j++)
                    if(*T[X+j][Y] != *simbolo[0]) //Comprueba si no choca
                        noChoque = false; //Si choca no deja continuar

                if(noChoque == true){
                    for(int i=0; i<longBarcos; i++)
                        *T[X+i][Y] = *simbolo[1]; //Vertical
                    direccion++;
                    longBarcos--;
                }
            }
        }

        //Cuarto Barco(Destructor)
        else if(direccion == 3){

            if(Y <= 5){
                for(int j=0; j<longBarcos; j++)
                    if(*T[X][Y+j] != *simbolo[0]) //Comprueba si no choca
                        noChoque = false; //Si choca no deja continuar

                if(noChoque == true){
                    for(int i=0; i<longBarcos; i++)
                        *T[X][Y+i] = *simbolo[1]; //Horizontal
                    direccion++;
                    longBarcos--;
                }
            }
        }

        //Quito Barco(Submarino)
        else if(direccion == 4){

            for(int j=0; j<longBarcos; j++)
                if(*T[X][Y] != *simbolo[0]) //Comprueba si no choca
                    noChoque = false; //Si choca no deja continuar

            if(noChoque == true){
                for(int i=0; i<longBarcos; i++)
                    *T[X][Y] = *simbolo[1];
                endR = true;
            }
        }
    }
}

void pregunta(){

    printf("Dime donde quieres disparar: ");
    scanf(" %i %i", &pos[0], &pos[1]);

    title();
    if(pos[0] >= DIM || pos[0] < 0 || pos[1] >= DIM || pos[1] < 0){
        printf("Es demasiado grande o pequeño.\n");
        pregunta();
    }
}

int main(){

    srand(time(NULL));
    char tablero[DIM][DIM], Mcomp[DIM][DIM];
    bool end = false, call = true;
    int intentos = 0, tocados = 14;

    relleno(tablero);
    relleno(Mcomp);

    //ponBarcos(Mcomp);

    do{
        imprimeTablero(tablero);
        printf("\n\n\n");
        imprimeTablero(Mcomp);

        pregunta();
        tablero[pos[0]][pos[1]] = *simbolo[1];
        printf("fu");
        printf(" %i, %i\n", pos[0], pos[1]);
        //pregunta();
        end = true;

        imprimeTablero(tablero);
        printf("\n\n\n");
        imprimeTablero(Mcomp);


    }while(end == false);

    return EXIT_SUCCESS;
}


