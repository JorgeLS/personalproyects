/* Comprueba si se ha dado a algún barco */
function comprobarBarco(){

	if(tablas[Cellx][Celly] == 0)
		return 0

	else if(tablas[Cellx][Celly] == 6)
		return 6

	else{
		for(var i=0; i<Portaaviones.length; i++){
			if(Portaaviones[i][0] == Cellx && Portaaviones[i][1] == Celly){
				tablas[Cellx][Celly] = 6
				return 5
			}
		}

		for(var i=0; i<Acorazado.length; i++){
			if(Acorazado[i][0] == Cellx && Acorazado[i][1] == Celly){
				tablas[Cellx][Celly] = 6
				return 4
			}
		}

		for(var i=0; i<Crucero.length; i++){
			if(Crucero[i][0] == Cellx && Crucero[i][1] == Celly){
				tablas[Cellx][Celly] = 6
				return 3
			}
		}
		
		for(var i=0; i<Destructor.length; i++){
			if(Destructor[i][0] == Cellx && Destructor[i][1] == Celly){
				tablas[Cellx][Celly] = 6
				return 2
			}
		}
	}
}

/*Comprueba si se ha hundido algún barco y si se destruye todos los barcos cambia la variable end */
function comprobarHundido(){

	var HITboat1 = true
	var HITboat2 = true
	var HITboat3 = true
	var HITboat4 = true

	for(var i=0; i<DIM; i++)
		if(tablas[i].includes(5) == true)
			HITboat1 = false

	for(var i=0; i<DIM; i++)
		if(tablas[i].includes(4) == true)
			HITboat2 = false

	for(var i=0; i<DIM; i++)
		if(tablas[i].includes(3) == true)
			HITboat3 = false

	for(var i=0; i<DIM; i++)
		if(tablas[i].includes(2) == true)
			HITboat4 = false

	if(HITboat1)
		document.getElementsByClassName("barcos")[0].innerHTML = "Portaaviones Hundido"
	if(HITboat2)
		document.getElementsByClassName("barcos")[1].innerHTML = "Acorazado Hundido"
	if(HITboat3)
		document.getElementsByClassName("barcos")[2].innerHTML = "Crucero Hundido"
	if(HITboat4)
		document.getElementsByClassName("barcos")[3].innerHTML = "Destructor Hundido"

	if(HITboat1 && HITboat2 && HITboat3 && HITboat4)
		end = true
}

/* Chequea si se quiere Emojis en el tablero o colores */
function CheckedEmojis(){
	emojis = document.getElementById("emojis").checked
	Clear()
}

/* Devuelve si se ha fallado o acertado */
function ponResultadoShoot(result){

		if(result == 0){
			document.getElementsByClassName("comp")[0].innerHTML = " Has fallado"
			if(emojis == true){
				tablero.font = "30px Arial"
				tablero.strokeText("🌊",50 + 15 + cubeDIM * Cellx, 50 + 40 + cubeDIM * Celly, cubeDIM, cubeDIM)
		    }
		    else{
		    	tablero.fillStyle = "#0000FF"
				tablero.fillRect(50 + cubeDIM * Cellx, 50 + cubeDIM * Celly, cubeDIM, cubeDIM)
		    }
		}
	
		else if(result == 6)
			document.getElementsByClassName("comp")[0].innerHTML = "Ya le habias dado al barco."
		
		else{
		    document.getElementsByClassName("comp")[0].innerHTML = "Le has dado"
		    tablero.fillStyle = "#FFFFFF"
			tablero.fillRect(50 + cubeDIM * Cellx, 50 + cubeDIM * Celly, cubeDIM, cubeDIM)
			tablero.strokeRect(50 + cubeDIM * Cellx, 50 + cubeDIM * Celly, cubeDIM, cubeDIM)
			if(emojis == true){
				tablero.font = "30px Arial"
				tablero.strokeText("🔥",50 + 15 + cubeDIM * Cellx, 50 + 40 + cubeDIM * Celly, cubeDIM, cubeDIM)
			}
			else{
				tablero.fillStyle = "#FF0000"
				tablero.fillRect(50 + cubeDIM * Cellx, 50 + cubeDIM * Celly, cubeDIM, cubeDIM)
			}
		}
}
