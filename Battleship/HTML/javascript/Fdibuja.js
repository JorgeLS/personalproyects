/* Dibuja el Tablero en el Canvas */
function dibujaTablero(){

	//Números
	tablero.beginPath()
	tablero.font = "25px Arial"
	tablero.fillStyle = "#000000"
	for(var i=0; i<DIM; i++)
		tablero.fillText(i, 70 + cubeDIM * i, 45)
	for(var i=0; i<DIM; i++)
		tablero.fillText(i, 30, 90 + cubeDIM * i)
	tablero.stroke()

	//Celdas
	//Normal
	tablero.beginPath()
	tablero.rect(50, 50, 600,600)
	for(var i=0; i<DIM; i++)
		for(var j=0; j<DIM; j++)
			dibuja(i, j, tablas[i][j])
	tablero.stroke()

	//Emojis
	tablero.beginPath()
	tablero.font = "30px Arial"
	for(var i=0; i<DIM; i++)
		for(var j=0; j<DIM; j++){
			if(emojis == true)
				tablero.strokeText("🌊",50 + 15 + cubeDIM * i, 50 + 40 + cubeDIM * j, cubeDIM, cubeDIM)
		}
	tablero.stroke()
}

/* Coje la posición y coordenadas, si hay barcos hundidos dibuja rojo y si no blanco */
function dibuja(x, y, pos){

	if(pos != 6)
		tablero.rect(50 + cubeDIM * x, 50 + cubeDIM * y, cubeDIM, cubeDIM)
	else{
		tablero.fillStyle = "#FF0000"
		tablero.fillRect(50 + cubeDIM * x, 50 + cubeDIM * y, cubeDIM, cubeDIM) 
	}

}

/* Al pulsar el botón de Mostrar su dibujan todos los Barcos*/
function dibujaBarcos(){

	for(var i=0; i<Portaaviones.length; i++){
		tablero.fillStyle = "#FFFFFF"
		tablero.fillRect(50 + cubeDIM * Portaaviones[i][0], 50 + cubeDIM * Portaaviones[i][1], cubeDIM, cubeDIM)
		tablero.strokeRect(50 + cubeDIM * Portaaviones[i][0], 50 + cubeDIM * Portaaviones[i][1], cubeDIM, cubeDIM)
		if(emojis == true){
			tablero.font = "30px Arial"
			tablero.strokeText("🚢",50 + 15 + cubeDIM * Portaaviones[i][0], 50 + 40 + cubeDIM * Portaaviones[i][1], cubeDIM, cubeDIM)
		}
		else{
			tablero.fillStyle = "#FF1111"
			tablero.fillRect(50 + cubeDIM * Portaaviones[i][0], 50 + cubeDIM * Portaaviones[i][1], cubeDIM, cubeDIM)
		}
	}

	for(var i=0; i<Acorazado.length; i++){
		tablero.fillStyle = "#FFFFFF"
		tablero.fillRect(50 + cubeDIM * Acorazado[i][0], 50 + cubeDIM * Acorazado[i][1], cubeDIM, cubeDIM)
		tablero.strokeRect(50 + cubeDIM * Acorazado[i][0], 50 + cubeDIM * Acorazado[i][1], cubeDIM, cubeDIM)
		if(emojis == true){
			tablero.font = "30px Arial"
			tablero.strokeText("🚤",50 + 15 + cubeDIM * Acorazado[i][0], 50 + 40 + cubeDIM * Acorazado[i][1], cubeDIM, cubeDIM)
		}
		else{
			tablero.fillStyle = "#FF1111"
			tablero.fillRect(50 + cubeDIM * Acorazado[i][0], 50 + cubeDIM * Acorazado[i][1], cubeDIM, cubeDIM)
		}
	}

	for(var i=0; i<Crucero.length; i++){
		tablero.fillStyle = "#FFFFFF"
		tablero.fillRect(50 + cubeDIM * Crucero[i][0], 50 + cubeDIM * Crucero[i][1], cubeDIM, cubeDIM)
		tablero.strokeRect(50 + cubeDIM * Crucero[i][0], 50 + cubeDIM * Crucero[i][1], cubeDIM, cubeDIM)
		if(emojis == true){
			tablero.font = "30px Arial"
			tablero.strokeText("🚣",50 + 15 + cubeDIM * Crucero[i][0], 50 + 40 + cubeDIM * Crucero[i][1], cubeDIM, cubeDIM)
		}
		else{
			tablero.fillStyle = "#FF1111"
			tablero.fillRect(50 + cubeDIM * Crucero[i][0], 50 + cubeDIM * Crucero[i][1], cubeDIM, cubeDIM)
		}
	}	

	for(var i=0; i<Destructor.length; i++){
		tablero.fillStyle = "#FFFFFF"
		tablero.fillRect(50 + cubeDIM * Destructor[i][0], 50 + cubeDIM * Destructor[i][1], cubeDIM, cubeDIM)
		tablero.strokeRect(50 + cubeDIM * Destructor[i][0], 50 + cubeDIM * Destructor[i][1], cubeDIM, cubeDIM)
		if(emojis == true){
			tablero.font = "30px Arial"
			tablero.strokeText("🚣",50 + 15 + cubeDIM * Destructor[i][0], 50 + 40 + cubeDIM * Destructor[i][1], cubeDIM, cubeDIM)
		}
		else{
			tablero.fillStyle = "#FF1111"
			tablero.fillRect(50 + cubeDIM * Destructor[i][0], 50 + cubeDIM * Destructor[i][1], cubeDIM, cubeDIM)
		}
	}
}