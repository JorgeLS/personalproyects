var DIM = 10
var cubeDIM = 60
var tablero
var Posx = 0
var Posy = 0
var Cellx
var Celly
var x
var y
var end = false
var emojis 

var tablas = [
	[0,0,0,0,0, 0,0,0,0,0],
	[0,0,0,0,0, 0,0,0,0,0],
	[0,0,0,0,0, 0,0,0,0,0],
	[0,0,0,0,0, 0,0,0,0,0],
	[0,0,0,0,0, 0,0,0,0,0],
	[0,0,0,0,0, 0,0,0,0,0],
	[0,0,0,0,0, 0,0,0,0,0],
	[0,0,0,0,0, 0,0,0,0,0],
	[0,0,0,0,0, 0,0,0,0,0],
	[0,0,0,0,0, 0,0,0,0,0]
]
var Portaaviones //= [[0,5],[0,6],[0,7],[0,8],[0,9]]
var Acorazado    //= [[5,9],[6,9],[7,9],[8,9]]
var Crucero      //= [[5,3],[5,4],[5,5]]
var Destructor   //= [[2,2],[2,3]]

/* Crea la posiciones de los barcos */
function creaBarcos(){

	for(var i=0; i<5; i++){
		if(i == 0){
			Portaaviones = new Array([0, i+5])
			tablas[0][i+5] = 5
		}
		else{
			Portaaviones.push([0, i+5])
			tablas[0][i+5] = 5
		}
	}
	for(var i=0; i<4; i++){
		if(i == 0){
			Acorazado = new Array([i+5, 9])
			tablas[i+5][9] = 4
		}
		else{
			Acorazado.push([i+5, 9])
			tablas[i+5][9] = 4
		}
	}
	for(var i=0; i<3; i++){
		if(i == 0){
			Crucero = new Array([5, i+3])
			tablas[5][i+3] = 3
		}
		else{
			Crucero.push([5, i+3])
			tablas[5][i+3] = 3
		}
	}
	for(var i=0; i<2; i++){
		if(i == 0){
			Destructor = new Array([2, i+2]) 
			tablas[2][i+2] = 2
		}
		else{
			Destructor.push([2, i+2])
			tablas[2][i+2] = 2
		}
	}
}

/* Hace un restart */
function Clear(){

	tablero.clearRect(0, 0, 670, 670)
	for(var i=0; i<10; i++)
  		tablas.pop();
	for(var i=0; i<10; i++)
  		tablas.push([0,0,0,0,0, 0,0,0,0,0]);
  	end = false
	dibujaTablero()
	creaBarcos()
	CojerCoords()
	document.getElementsByClassName("barcos")[0].innerHTML = "Portaaviones en Pie"
	document.getElementsByClassName("barcos")[1].innerHTML = "Acorazado en Pie"
	document.getElementsByClassName("barcos")[2].innerHTML = "Crucero en Pie"
	document.getElementsByClassName("barcos")[3].innerHTML = "Destructor en Pie"
}

/* Coje las coordenadas del Menú de Disparo */
function CojerCoords(){

	x = parseInt(document.getElementsByClassName("Slider")[0].value)
	y = parseInt(document.getElementsByClassName("Slider")[1].value)

	Cellx = x
	Celly = y

	document.getElementsByClassName("CoordenadaX")[0].innerHTML = "Coordenadas X = " + Cellx
	document.getElementsByClassName("CoordenadaY")[0].innerHTML = "Coordenadas Y = " + Celly
	document.getElementsByClassName("comp")[0].innerHTML = "Elige y dispara"
}

/* Funcion que se activa al pulsar Shoot y  */
function Shoot(){

	if (end == false){
		
		CojerCoords()
		ponResultadoShoot( comprobarBarco() )
		//Comprueba si se ha hundido algún barco
		comprobarHundido()
		//Si se han destruido todos los barcos dibuja has ganado 
		Final()
	}
}

/* Si end = true Dibuja has ganado */
function Final(){

	if(end){
		tablero.font = "75px Arial"
		tablero.fillText("HAS GANADO", 100, 290)
	}
}

/* Función Principal que se ejecuta al cargar la página */
function main(){

	tablero = document.getElementById("tablero").getContext("2d")

	CheckedEmojis()
	dibujaTablero()
	creaBarcos()
	CojerCoords()
}