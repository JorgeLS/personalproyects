#include <stdio.h>
#include <stdlib.h>

#define F 4
#define DimF 6
#define DimC 7
#define END -1

void title(){

    system("clear");
    printf("\n");
    system("toilet -fpagga -Fgay:border '    4 en Raya    '");
    printf("\n\n");
}

void tablero(char cells[6][7], int turno){

    title();

    printf("\t  1   2   3   4   5   6   7\n");
    printf("\t ―――――――――――――――――――――――――――\n");
    for(int i=0; i<DimF; i++){
        if(i != 0)
            printf("\t|―――|―――|―――|―――|―――|―――|―――|\n");
        for(int j=0; j<DimC; j++){
            if(j == 0)
                printf("\t|");
            printf(" %c |",cells[i][j]);
        }
        if(i != 5)
            printf("\n");
    }
    printf("\n\t ―――――――――――――――――――――――――――\n\n");
}

int coje_columna(int columna, int fila[7]){

    printf("\nDime en que columna quieres meter la siguiente ficha[1-7]: ");
    scanf(" %i", &columna);

    columna--;

    if(columna > 6 || columna < 0){
        printf("\nEse valor es demasiado pequeño o demasiado grande.");
        coje_columna(columna, fila);
    }
    else if(fila[columna] < 0){
        printf("\nEn esta columna ya hay todas las fichas, escoje otra.");
        coje_columna(columna, fila);
    }
    else
        return columna;
}

void ponFicha(char cells[6][7], int columna, int fila[6], char color){

    cells[fila[columna]][columna] = color;

    fila[columna]--;
}
bool compruebaFila(int fila[7]){

    if(fila[0] == END && fila[1] == END && fila[2] == END &&
            fila[3] == END && fila[4] == END && fila[5] == END &&
            fila[6] == END)
        return true;
    else
        return false;
}

bool comprueba(char celdas[6][7], char nada){

    /* Columnas */
    for(int i=-1; i<6; i++)
        for(int j=0; j<4; j++)
            if(celdas[i][j] == celdas[i][j+1] && celdas[i][j] == celdas[i][j+2] &&
                    celdas[i][j] == celdas[i][j+3] && celdas[i][j] != nada)
                return true;
    /* Filas */
    for(int i=0; i<7; i++)
        for(int j=-1; j<3; j++)
            if(celdas[j][i] == celdas[j+1][i] && celdas[j][i] == celdas[j+2][i] &&
                    celdas[j][i] == celdas[j+3][i] && celdas[j][i] != nada)
                return true;
    /* Diagonal Ascendente */
    for(int i=0; i<3; i++)
        for(int j=0; j<4; j++)
            if(celdas[i][j+3] == celdas[i+1][j+2] && celdas[i][j+3] == celdas[i+2][j+1] &&
                    celdas[i][j+3] == celdas[i+3][j] && celdas[i][j+3] != nada)
                return true;
    /* Diagonal Descendente */
    for(int i=3; i>0; i--)
        for(int j=0; j<4; j++)
            if(celdas[i][j] == celdas[i+1][j+1] && celdas[i][j] == celdas[i+2][j+2] &&
                    celdas[i][j] == celdas[i+3][j+3] && celdas[i][j] != nada)
                return true;
    return false;
}


void fin(char celdas[6][7], int turno){

    tablero(celdas, turno);

    if(turno % 2 != 0)
        system("toilet -fpagga -F border:metal 'Han Ganado las X'");
    else
        system("toilet -fpagga -F border:metal 'Han Ganado los O'");
}

int main(){

    int columa, fila[7] = {5,5,5,5,5,5,5};
    int turno = 0;
    char N = ' ';
    char O = 'O';
    char X = 'X';
    char celdas[6][7];

    // Inicializacion del tablero
    for(int i=0; i<6; i++)
        for(int j=0; j<7; j++)
            celdas[i][j] = N;

    do{
        // Dibuja tablero
        tablero(celdas, turno);

        if(turno % 2 == 0)
            system("toilet -fpagga -F crop 'Turno de las X'");
        else
            system("toilet -fpagga -F crop 'Turno de los O'");
        // Llama a la columna
        columa = coje_columna(columa, fila);

        // Introduce X o O dependiendo del turno
        if(turno % 2 == 0)
            ponFicha(celdas, columa, fila, X);
        else
            ponFicha(celdas, columa, fila, O);

        turno++;

        // Comprueba si tablero esta lleno
        // compruebaFila(fila);

        // Comprueba si algun jugador ha ganado
        // comprueba(celdas, N);
    }while( !compruebaFila(fila) && !comprueba(celdas, N) );

    fin(celdas, turno);

    return EXIT_SUCCESS;
}
