using System;

namespace _4enRaya
{
    class Program
    {
        static void Main(string[] args)
        {
            int turno = 0, columna = 0;
            char[,] tablero = new char[6, 7];
            char X = 'X', O = 'O';
            int[] filas = new int[] { 5, 5, 5, 5, 5, 5, 5 };

            // Inicializa el tablero
            InicializarTablero(ref tablero);

            do
            {
                Console.WriteLine(TableroVisual(tablero));

                // Turno de los jugadores
                if (turno % 2 == 0)
                    Console.WriteLine("Turno del Jugador 1");
                else
                    Console.WriteLine("Turno del Jugador 2");

                // Lee la columna que se quiere poner
                LecturaDatos(ref tablero, filas, turno, X, O, ref columna);

                turno++;

            } while ( !Ganador(tablero, filas, columna) );

            // Dibuja el final
            Console.WriteLine( TableroVisual(tablero) );

            if (turno %2 != 0)
                Console.WriteLine("\t    El jugador 1 ha ganador\n");
            else
                Console.WriteLine("\t    El jugador 2 ha ganado\n");
        }

        static void LecturaDatos(ref char[,] tablero, int[] filas, int turno, char X, char O, ref int c)
        {
            Console.WriteLine("Introduce el número de la columna: ");

            string l = Console.ReadLine();

            while ( !int.TryParse(l, out c) )
            {

                Console.WriteLine("Vuelve a meter el numero pero esta vez bien porfavor.");
                l = Console.ReadLine();
            }
            c--;

            ComprovacionDatos(ref tablero, filas, turno, X, O, ref c);
        }

        static void ComprovacionDatos(ref char[,] tablero, int[] filas, int turno, char X, char O, ref int c)
        {

            if (c < 0 || c > 6)
            {
                Console.WriteLine("No se pueden colocar fichas ahí");
                LecturaDatos(ref tablero, filas, turno, X, O, ref c);
            }
            else
            {
                if (filas[c] > -1)
                {
                    if (turno % 2 == 0)
                    {
                        tablero[filas[c], c] = X;
                    }
                    else
                    {
                        tablero[filas[c], c] = O;
                    }
                    filas[c]--;
                }
                else
                {
                    Console.WriteLine("No se pueden colocar más fichas");
                    LecturaDatos(ref tablero, filas, turno, X, O, ref c);
                }
            }
        }

        static void InicializarTablero(ref char[,] tablero)
        {
            for (int fila = 0; fila < 6; fila++)
            {
                for (int columna = 0; columna < 7; columna++)
                {
                    tablero[fila, columna] = ' ';
                }
            }
        }

        static string TableroVisual(char[,] tablero)
        {
            string tv = "";

            Console.Clear();
            Console.WriteLine("\n\t\t   4 en RAYA\n");

            tv = "\t   1   2   3   4   5   6   7" + Environment.NewLine;
            tv +=  "\t ┌───┬───┬───┬───┬───┬───┬───┐" + Environment.NewLine;
            tv += $"\t | {tablero[0, 0]} | {tablero[0, 1]} | {tablero[0, 2]} | {tablero[0, 3]} | {tablero[0, 4]} | {tablero[0, 5]} | {tablero[0, 6]} |" + Environment.NewLine;
            tv +=  "\t ├───┼───┼───┼───┼───┼───┼───┤" + Environment.NewLine;
            tv += $"\t | {tablero[1, 0]} | {tablero[1, 1]} | {tablero[1, 2]} | {tablero[1, 3]} | {tablero[1, 4]} | {tablero[1, 5]} | {tablero[1, 6]} |" + Environment.NewLine;
            tv +=  "\t ├───┼───┼───┼───┼───┼───┼───┤" + Environment.NewLine;
            tv += $"\t | {tablero[2, 0]} | {tablero[2, 1]} | {tablero[2, 2]} | {tablero[2, 3]} | {tablero[2, 4]} | {tablero[2, 5]} | {tablero[2, 6]} |" + Environment.NewLine;
            tv +=  "\t ├───┼───┼───┼───┼───┼───┼───┤" + Environment.NewLine;
            tv += $"\t | {tablero[3, 0]} | {tablero[3, 1]} | {tablero[3, 2]} | {tablero[3, 3]} | {tablero[3, 4]} | {tablero[3, 5]} | {tablero[3, 6]} |" + Environment.NewLine;
            tv +=  "\t ├───┼───┼───┼───┼───┼───┼───┤" + Environment.NewLine;
            tv += $"\t | {tablero[4, 0]} | {tablero[4, 1]} | {tablero[4, 2]} | {tablero[4, 3]} | {tablero[4, 4]} | {tablero[4, 5]} | {tablero[4, 6]} |" + Environment.NewLine;
            tv +=  "\t ├───┼───┼───┼───┼───┼───┼───┤" + Environment.NewLine;
            tv += $"\t | {tablero[5, 0]} | {tablero[5, 1]} | {tablero[5, 2]} | {tablero[5, 3]} | {tablero[5, 4]} | {tablero[5, 5]} | {tablero[5, 6]} |" + Environment.NewLine;
            tv +=  "\t └───┴───┴───┴───┴───┴───┴───┘" + Environment.NewLine; ;

            return tv;
        }

        static bool Ganador(char[,] celdas, int[] fila, int columna)
        {
            // Check all cells
            for (int i = 0; i < 6; i++)
                for (int j = 0; j < 4; j++)
                    if (celdas[i, j] == celdas[i, j + 1] && celdas[i, j] == celdas[i, j + 2] &&
                            celdas[i, j] == celdas[i, j + 3] && celdas[i, j] != ' ')
                        return true;

            for (int i = 0; i < 7; i++)
                for (int j = 0; j < 3; j++)
                    if (celdas[j, i] == celdas[j + 1, i] && celdas[j, i] == celdas[j + 2, i] &&
                            celdas[j, i] == celdas[j + 3, i] && celdas[j, i] != ' ')
                        return true;

            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 4; j++)
                    if (celdas[i, j + 3] == celdas[i + 1, j + 2] && celdas[i, j + 3] == celdas[i + 2, j + 1] &&
                            celdas[i, j + 3] == celdas[i + 3, j] && celdas[i, j + 3] != ' ')
                        return true;

            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 4; j++)
                    if (celdas[i, j] == celdas[i + 1, j + 1] && celdas[i, j] == celdas[i + 2, j + 2] &&
                            celdas[i, j] == celdas[i + 3, j + 3] && celdas[i, j] != ' ')
                        return true;
            return false;
        }
    }
}
