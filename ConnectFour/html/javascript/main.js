var Filas = 6*100
var filas = 6 
var Columnas = 7*100
var columnas = 7
var columi
var lienzo
var turno = 0
var end = false
var R = "rojo"
var A = "amarillo"
var B = "blanco"
var celdas = [0,7,7,7,7,7,7,7]
var cells = new Array(
0,
new Array(0,1,1,1,1,1,1),
new Array(0,1,1,1,1,1,1),
new Array(0,1,1,1,1,1,1),
new Array(0,1,1,1,1,1,1),
new Array(0,1,1,1,1,1,1),
new Array(0,1,1,1,1,1,1),
new Array(0,1,1,1,1,1,1)
)


function color(i){

	if(i == 0) //white
		return "#FFFFFF"
	else if(i == 1) //black
		return "#111111"
	else if(i == 2) //red
		return "#FF0000"
	else if(i == 3) //yellow
		return "#FFFF00"
	else if(i == 4) //blue
		return "#3311FF"
	else
		return "#00FF00"
}

function tablero(){

	lienzo.beginPath()
	lienzo.fillStyle = color(4)
	lienzo.strokeStyle = color(1)
	for (var i = 0; i < Filas; i+=100){
		for (var j = 0; j < Columnas; j+=100){
			lienzo.fillRect(j + j/10 + 10, i + 10, 110, 100)
			lienzo.strokeRect(j + j/10 + 10, i + 10, 110, 100)
		}
	}
	lienzo.stroke()
}
function circulo(r1, r2, color){

	lienzo.fillStyle = color
	lienzo.strokeStyle = color
	lienzo.beginPath()
	lienzo.arc(r1, r2, 45, 0, 2 * Math.PI)
	lienzo.stroke()
	lienzo.fill()

}

function meterFicha(x){

	if (end == false){

		if (celdas[x] > 0)
			celdas[x]--

		if (celdas[x] > 0){
			if  ( turno  % 2 == 0){
				Ficha(celdas[x], x, R)
				cells[x][celdas[x]] = R;
		}
		else{
			Ficha(celdas[x], x, A)
			cells[x][celdas[x]] = A;
		}
		turno++
	}
	comprueba()
	turns()
	}
}

function Ficha(fil, colum, col){

	fil--
	colum--
	colum *= 100

	if (col == "rojo")
		for (var i = fil * 100; i < Filas; i+=100){
			if(i == fil * 100)
				circulo(colum + colum/10 + 65,i + 60,color(2))
		}
	else if (col == "amarillo")
		for (var i = fil * 100; i < Filas; i+=100){
			if(i == fil * 100)
				circulo(colum + colum/10 + 65,i + 60,color(3))
		}
}

function borrar(){

	lienzo.clearRect(0, 0, 800, 615)

	tablero()

	for (var i = 0; i < Filas; i+=100)
		for (var j = 0; j < Columnas; j+=100)
			circulo(j+ j/10 + 65, i + 60, color(0))

	for (var i = 0; i < celdas.length; i++)
		celdas[i] = 7

	for (var i = 0; i< cells.length; i++)
		for (var j = 0; j < cells.length; j++)
			cells[i][j] = 1

	turno = 0
	turns()

	end = false
	document.getElementById("fin").innerHTML = ""
}

function turns(){

	if(turno % 2 == 0)
		document.getElementsByClassName("turno")[0].innerHTML = "Turno de los Rojos"
	else
		document.getElementsByClassName("turno")[0].innerHTML = "Turno de los Amarillos"
}

function comprueba(){

	/* Columnas */
	for (var i = 1; i < 8; i++)
        for (var j = 1; j < 4; j++)
            if (cells[i][j] == cells[i][j+1] && cells[i][j] == cells[i][j+2] &&
                    cells[i][j] == cells[i][j+3] && cells[i][j] != 1)
                end = true

    /* Filas */
    for (var i = 1; i < 7; i++)
        for (var j = 1; j < 5; j++)
            if (cells[j][i] == cells[j+1][i] && cells[j][i] == cells[j+2][i] &&
                    cells[j][i] == cells[j+3][i] && cells[j][i] != 1)
                end = true

    /* Diagonal Ascendente */
    for (var i = 1; i < 5; i++)
        for (var j = 1; j < 5; j++)
            if (cells[i][j+3] == cells[i+1][j+2] && cells[i][j+3] == cells[i+2][j+1] &&
                    cells[i][j+3] == cells[i+3][j] && cells[i][j+3] != 1)
                end = true

    /* Diagonal Descendente */
    for (var i = 1; i < 5; i++)
        for (var j = 1; j < 4; j++)
            if (cells[i][j] == cells[i+1][j+1] && cells[i][j] == cells[i+2][j+2] &&
                    cells[i][j] == cells[i+3][j+3] && cells[i][j] != 1)
                end = true
    /* Final en Empate */
    if(celdas[1] == celdas[2] && celdas[1] == celdas[3] && celdas[1] == celdas[4] &&
    	celdas[1] == celdas[5] && celdas[1] == celdas[6] && celdas[1] == celdas[7] &&
    	celdas[1] == 0){
    	document.getElementById("fin").innerHTML = "Han quedado empate"
    	end = true
    }
    else if(end == true)
		if(turno % 2 != 0)
			document.getElementById("fin").innerHTML = "Han ganado los Rojos"
		else
			document.getElementById("fin").innerHTML = "Han ganado los Amarillos"
}

function main(){

	lienzo = document.getElementById("lienzo").getContext("2d")


	/* Dibuja el tablero */
	tablero()
	
	/* Dibuja los circulos blancos */
	for (var i = 0; i < Filas; i+=100)
		for (var j = 0; j < Columnas; j+=100)
			circulo(j+ j/10 + 65, i + 60, color(0))

	/* Pone el turno */
	turns()
	comprueba()
}