#include <stdio.h>
#include <stdlib.h>
#include <curses.h>

#define DIM 9

int main() {

    int posX = 10, posY = 10;
    char h[] = " Hola H";
    int m[] = {1, 2, 3, 6, 5, 7, 8, 6, 7};
    int M[9][9] = {
        {1, 2, 1, 6, 5, 7, 8, 6, 7},
        {2, 0, 3, 2, 7, 5, 4, 2, 7},
        {3, 5, 1, 6, 3, 6, 6, 5, 7},
        {4, 2, 3, 9, 5, 8, 8, 6, 4},
        {5, 4, 5, 6, 3, 9, 9, 6, 6},
        {6, 4, 3, 8, 7, 7, 6, 7, 5},
        {7, 2, 5, 3, 5, 5, 6, 7, 7},
        {8, 2, 3, 6, 3, 7, 9, 6, 6},
        {9, 2, 1, 4, 7, 9, 8, 6, 8}
    };

    initscr();

    for(int i = 0; i<DIM; i++)
        for(int j = 0; j<DIM; j++) {
            if( i == 0 || i == DIM-1 )
                mvprintw(posY + i, posX + j, "-");
            else if( j == 0 || j == DIM-1 )
                mvprintw(posY + i, posX + j, "|");
        }

    posX = 30;
    posY = 10;

    for(int i = 0; i<DIM; i++)
        for(int j = 0; j<DIM; j++) {
            if( i == 0 || i == DIM-1 )
                mvprintw(posY + i, posX + j, "-");
            else if( j == 0 || j == DIM-1 )
                mvprintw(posY + i, posX + j, "|");
            else
                mvprintw(posY + i, posX + j, "%c", h[j]);
        }

    posX = 10;
    posY = 30;

    for(int i = 0; i<DIM; i++)
        for(int j = 0; j<DIM; j++) {
            if( i == 0 || i == DIM-1 )
                mvprintw(posY + i, posX + j, "-");
            else if( j == 0 || j == DIM-1 )
                mvprintw(posY + i, posX + j, "|");
            else
                mvprintw(posY + i, posX + j, "%i", m[j]);
        }

    posX = 30;
    posY = 30;

    for(int i = 0; i<DIM; i++)
        for(int j = 0; j<DIM; j++) {
            if( i == 0 || i == DIM-1 )
                mvprintw(posY + i, posX + j, "-");
            else if( j == 0 || j == DIM-1 )
                mvprintw(posY + i, posX + j, "|");
            else
                mvprintw(posY + i, posX + j, "%i", M[i][j]);
        }


    getch();
    erase();
    getch();
    endwin();
}
