var width = 340
var height = 340
var XC = width/2
var YC = height/2
var Dim=3
var lienzo
var turno = 0
var winner = N
var end = false

/* Nº Of Positions */
var zero = 20
var one = zero + 100
var two = zero + 100 * 2

/* Dibujo de las Posiciones */
var X = "X"
var O = "O"
var N = " "

/* Cells */
var celda = [zero, one, two]
var dibujo = [
N, N, N,
N, N, N,
N, N, N]
var nada = [
N,N,N,
N,N,N,
N,N,N]

function borrar(){

    for(var i=0; i<Dim*Dim; i++)
        dibujo[i] = nada[i]
    dibuja(dibujo)
    turno = 0
    end = false
    winner = N
    document.getElementsByClassName("turno")[0].innerHTML = "TURNO DE: " + X
    document.getElementById("winner").innerHTML = " "
}

function dibuja(tablero){

    lienzo.clearRect(0 , 0, width, height)

    /* Draw Cells */
    lienzo.beginPath()
    for(var i=0; i<Dim; i++)
        for(var j=0; j<Dim; j++)
            lienzo.rect(celda[i], celda[j], 100, 100)
    lienzo.stroke()

    /* Draw Caracters */
    lienzo.beginPath()
    lienzo.font = "30px Arial"
    for(var i=0, z=0; i<Dim; i++){
        for(var j=0; j<Dim; j++){
            lienzo.fillText(tablero[z], celda[j] + 40, celda[i] + 60)
            z++;
        }
    }
    lienzo.stroke()

    comprueba()
 }

 function change(i){
    
    if(end == false){
        var n = i-1
        if(document.getElementsByClassName("button")[n] != 10){
            if(turno % 2 == 0 && dibujo[n] == N){
                dibujo[n] = X
                turno++
            }
            else if(dibujo[n] == N){
                dibujo[n] = O
                turno++
            }
        }
        dibuja(dibujo)
        if (turno % 2 == 0)
            document.getElementsByClassName("turno")[0].innerHTML = "TURNO DE: " + X
        else
            document.getElementsByClassName("turno")[0].innerHTML = "TURNO DE: " + O
    }
}

function comprueba(){

    /* DIAGONALES */
    if( dibujo[0] == dibujo[4] && dibujo[0] == dibujo[8] && dibujo[0] != N )
        winner = dibujo[0] 
    else if( dibujo[2] == dibujo[4] && dibujo[2] == dibujo[6] && dibujo[2] != N )
        winner = dibujo[2]
    /* COLUMNA */
    else if( dibujo[0] == dibujo[3] && dibujo[0] == dibujo[6] && dibujo[0] != N )
        winner = dibujo[0]
    else if( dibujo[1] == dibujo[4] && dibujo[1] == dibujo[7] && dibujo[1] != N )
        winner = dibujo[1]
    else if( dibujo[2] == dibujo[5] && dibujo[2] == dibujo[8] && dibujo[2] != N )
        winner = dibujo[2]
    /* FILAS */
    else if( dibujo[0] == dibujo[1] && dibujo[0] == dibujo[2] && dibujo[0] != N )
        winner = dibujo[0]
    else if( dibujo[3] == dibujo[4] && dibujo[3] == dibujo[5] && dibujo[3] != N )
        winner = dibujo[3]
    else if( dibujo[6] == dibujo[7] && dibujo[6] == dibujo[8] && dibujo[6] != N )
        winner = dibujo[6]

    if (winner == X){
        document.getElementById("winner").innerHTML = "Han Ganado las " + X
        end = true
    }
    else if(winner == O){
        document.getElementById("winner").innerHTML = "Han Ganado los " + O
        end = true
    }
    else if(turno == 9)
        document.getElementById("winner").innerHTML = "Han quedado empate"
}

function main(){

    lienzo = document.getElementById("lienzo").getContext("2d")
    
    document.getElementsByClassName("turno")[0].innerHTML = "TURNO DE: " + X
    dibuja(dibujo)
}
