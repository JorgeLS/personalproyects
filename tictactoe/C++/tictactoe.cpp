#include <stdio.h>
#include <stdlib.h>

#define DIM 3
#define ERROR 0

/* PONE EL TITULO */
void titulo(){

    system("clear");
    system("toilet -fpagga --filter border:crop 'TRES EN RAYA'");
}

/* DIBUJA EL TABLERO */
void tablero(char celda[DIM][DIM]){

    printf("\nFila → \nColumna↓\n"
            "\t ─────────────────\n");
    for(int fila=0; fila<DIM; fila++){
        printf("\t");
        for(int colum=0; colum<DIM; colum++)
            printf("|  %c  ",celda[fila][colum]);
        printf("|\n");
        if(fila != DIM - 1)
            printf("\t|-----|-----|-----|\n");
    }
    printf("\t ─────────────────\n\n\n");
}

void juego(char celda[DIM][DIM]){

    titulo();
    tablero(celda);
}

/* POSICION DE LAS O */
void posO(int fila, int colum, char celda[DIM][DIM]){

    printf("\n\tDime, la posición del siguiente movimiento (fila,columna): ");
    scanf(" %d %d", &fila, &colum);

    if(celda[fila-1][colum-1] == ' ')
        celda[fila-1][colum-1] = 'O';

    else if(fila > 3 || colum > 3 || fila < 1 || colum < 1){
        printf("\tNo puedes poner valores mayores que 3\n"
                "\tTampoco menores que 0\n");
        posO(fila,colum,celda);
    }
    else{
        printf("\tEsa celda ya esta cogida pon otra\n");
        posO(fila,colum,celda);
    }
}

/* POSICION DE LAS X */
void posX(int fila, int colum, char celda[DIM][DIM]){

    printf("\n\tDime, la posición del siguiente movimiento (fila,columna): ");
    scanf(" %d %d", &fila, &colum);

    if(celda[fila-1][colum-1] == ' ')
        celda[fila-1][colum-1] = 'X';

    else if(fila > 3 || colum > 3 || fila < 1 || colum < 1){
        printf("\tNo puedes poner valores mayores que 3\n"
                "\tTampoco menores que 0\n");
        posX(fila,colum,celda);
    }
    else{
        printf("\tEsa celda ya esta cogida pon otra\n");
        posX(fila,colum,celda);
    }
}

/* DICE SI GANAS O NO */
bool ganador(char celda[DIM][DIM]){

    /* DIAGONALES */
    if( celda[0][0] == celda[1][1] && celda[0][0] == celda[2][2] && celda[0][0] != ' ' )
        return true;
    else if( celda[0][2] == celda[1][1] && celda[0][2] == celda[2][1] && celda[0][2] != ' ' )
        return true;
    /* COLUMNA */
    else if( celda[0][0] == celda[1][0] && celda[0][0] == celda[2][0] && celda[0][0] != ' ' )
        return true;
    else if( celda[0][1] == celda[1][1] && celda[0][1] == celda[2][1] && celda[0][1] != ' ' )
        return true;
    else if( celda[0][2] == celda[1][2] && celda[0][2] == celda[2][2] && celda[0][2] != ' ' )
        return true;
    /* FILAS */
    else if( celda[0][0] == celda[0][1] && celda[0][0] == celda[0][2] && celda[0][0] != ' ' )
        return true;
    else if( celda[1][0] == celda[1][1] && celda[1][0] == celda[1][2] && celda[1][0] != ' ' )
        return true;
    else if( celda[2][0] == celda[2][1] && celda[2][0] == celda[2][2] && celda[2][0] != ' ' )
        return true;
    /* DEFAULT */
    else
        return false;
}

int main(){

    int fila, colum;
    int turno = 0;
    char circ = 'O', cruz = 'X', nada = ' ';
    char celda[DIM][DIM] = {
        {nada,nada,nada},
        {nada,nada,nada},
        {nada,nada,nada}};
    bool end = false;
    char winner = ' ';

    do{
        juego(celda);

        if(turno != 9){

            if(turno % 2 == 0){
                system("toilet -fpagga --metal \"TURNO DE O\"");
                posO(fila, colum, celda);
            }
            else{
                system("toilet -fpagga --metal \"TURNO DE X\"");
                posX(fila, colum, celda);
            }

            end = ganador(celda);

            if( end == true){
                if(turno % 2 == 0)
                    winner = 'O';
                else
                    winner = 'X';
            }
        }
        else
            end = true;

        if(end == false)
            turno++;

    }while(end != true);

    if(winner == ' '){
        juego(celda);
        system("toilet -f smblock --gay 'HAN QUEDADO EMPATE'");
        printf("\n\n");
    }
    else if(winner == 'O'){
        juego(celda);
        system("toilet -f smblock --gay 'HA GANADO LOS O FELICIDADES!!!!'");
        printf("\n\n");
    }
    else{
        juego(celda);
        system("toilet -f smblock --gay 'HA GANADO LAS X FELICIDADES!!!!'");
        printf("\n\n");
    }

    return EXIT_SUCCESS;
}
