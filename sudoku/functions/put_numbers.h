#ifndef _PUT_NUMBERS_H_
#define _PUT_NUMBERS_H_

// Check if the number are good
bool check_numbers (int M[DIM][DIM], int x, int y, int num) {

    if(M[x][y] == 0) {
        // Check the horizontal and vertical
        for(int i=0; i<DIM; i++){
            if(M[x][i] == num)
                return false;
            if(M[i][y] == num)
                return false;
        }

        // Check each cell
        for(int pos_x = 0; pos_x < 3; pos_x++) {
            for(int pos_y = 0; pos_y < 3; pos_y++) {
                // This 'if' is so you do not check itself
                if(pos_x == (x % 3) && pos_y == (y % 3)) continue;
                if(M[x - (x % 3) + pos_x][y - (y % 3) + pos_y] == num) {
                    // Can be also x + (x%3) + pos_x and y + (y%3) + pos_y
                    return false;
                }
            }
        }
        return true;
    }
    else
        return false;
}

// Put random numbers to create the sudoku
void put_numbers (int M[DIM][DIM]) {

    int t;
    bool end;

    // Put numbers one by one
    int num = 1;
    int c;

    do {
        for(int f = 0; f < DIM; f++) {
            // This variable serves so that an infinite loop is not created
            t=0;
            do {
                c = rand() % DIM;
                end = true;
                if ( check_numbers(M, f, c, num) )
                    M[f][c] = num;
                else {
                    t++;
                    end = false;
                }
                if(t > 150)
                    end = true;
            } while(!end);
        }
        num++;
    }while(num < 10);
/*
    // Put the numbers by row
    int num = 0;

    for(int i=0; i<DIM; i++){
        for(int j=0; j<DIM; j++){
            t = 0;
            do{
                num = rand() % 9 + 1; //Coje de [1-9]
                end = true;
                if( check_numbers(M, i, j, num) )
                    board[i][j] = num;
                else{
                    t++;
                    end = false;
                }
                if(t > 150)
                    end = true;
            }while(!end);
        }
    }

    // Put the numbers by cells
    int num;
    int F = 0, C = 0;

    for(int e = 0; e<DIM; e++){
        for(int i = 3 * F; i<dim; i++){
            for(int j = 3 * C; j<dim; j++){
                t=0;
                do{
                    num = rand() % 9 + 1;
                    end = true;
                    if( check_numbers(M, i, j, num) )
                        board[i][j] = num;
                    else{
                        end = false;
                        t++;
                    }
                    if(t > 150)
                        end = true;
                }while(!end);
            }
        }
        if(e % 3 == 0 && e != 0){
            F++;
            C = 0;
        }
        else
            C++;
    }
*/

}

#endif
