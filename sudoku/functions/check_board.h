#ifndef _CHECK_BOARD_H_
#define _CHECK_BOARD_H_

// Check if all the numbers have been set
bool check_board (int M[DIM][DIM]) {

    for(int i=0; i<DIM; i++)
        for(int j=0; j<DIM; j++)
            if(M[i][j] == 0)
                return false;
    return true;
}

// Check if the sudoku it is okay
bool check_same () {

    for(int i=0; i<DIM; i++)
        for(int j=0; j<DIM; j++)
            if( board[i][j] != initial_board[i][j] )
                return false;
    return true;
}

#endif
