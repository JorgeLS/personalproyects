#ifndef _MATRIX_TO_RESOLVE_H_
#define _MATRIX_TO_RESOLVE_H_

void matrix_saved();

// Create the sudoku if is new
void create_sudoku (int M[DIM][DIM]) {

    bool end;

    do{
        put_numbers(M);
        if( check_board(M) )
            end = true;
        else{
            bzero(M, sizeof(int)*DIM*DIM);
            end = false;
        }
    }while(!end);
}

// Create a new matriz to put on board
void matrix_new () {

    // Fill the board
    create_sudoku (initial_board);

    for(int i=0; i<DIM; i++)
        for(int j=0; j<DIM; j++)
            board[i][j] = initial_board[i][j];

    set_level ( get_level() );
    draw ();
}

// Create the sudoku to resolve
void matrix_to_resolve () {

    int choice;

    titulo();
    printf("\n\tWhat do you choose?:\n\n"
           "\t  1.- New sudoku\n"
           "\t  2.- Saved sudoku\n"
           "\n\tWrite the number: ");
    scanf(" %i", &choice);

    switch(choice){
        case 1:
            matrix_new ();
            break;
        case 2:
            matrix_saved ();
            break;
        default:
            printf("This election is invalid\n");
            matrix_to_resolve ();
            break;
    }
}

// Read the matrix saved in saves.dat
void matrix_saved () {

    FILE *pf;

    if ( !(pf = fopen(FILENAME, "rb")) ) {
        printf("There is no saved sudoku.\n");
        matrix_to_resolve();
    }

    fread(board, sizeof(int), sizeof(board), pf);

    fclose(pf);

    draw ();
}

#endif
