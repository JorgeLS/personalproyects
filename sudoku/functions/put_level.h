#ifndef _PUT_LEVEL_H_
#define _PUT_LEVEL_H_

// Create the level of the sudoku
void set_level (int level) {

    int F, C, num;

    if( level == 1 )
        num = rand () % 10 + 40;
    else if( level == 2 )
        num = rand () % 20 + 50;
    else if ( level == 3 )
        num = rand () % 30 + 60;
    else
        num = 666;

    for(int i=0; i<num; i++){
        F = rand () % DIM;
        C = rand () % DIM;
        board[F][C] = 0;
    }
}

// Get the level for the sudoku
int get_level () {

    int level;

    printf("\n\tSay me the level of the sudoku:\n\n"
            "\t  1.- Easy \n "
            "\t  2.- Medium \n "
            "\t  3.- Dificult \n"
            "\n\tWrite the number: ");
    scanf(" %i", &level);

    if( level > 3 && level != 666 ){
        printf("\n\tThis level not exists.\n");
        get_level();
    }

    return level;
}

#endif
