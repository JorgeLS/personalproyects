#ifndef _SAVE_GAME_H_
#define _SAVE_GAME_H_

// Save the sudoku in the file saves.dat
void save () {

    FILE *pf;

    if( !(pf = fopen (FILENAME, "wb")) )
        fprintf(stderr, "Douch! File not found\n");

    fwrite(board, sizeof(int), sizeof(board), pf);

    fclose(pf);
}

#endif
