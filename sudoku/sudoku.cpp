#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

// Here are the variables
#include "general.h"

int main (int argc, char *argv[]) {

    // Seed of the random
    srand(time(NULL));

    // Put the initial_board to 0
    bzero (initial_board, sizeof(initial_board));

    // Create a board to solve it
    matrix_to_resolve ();

    // Loop to ask and put numbers on board
    while( !(check_board(board)) ) {

        // If you want to exit menu = true and the program end
        if ( menu () )
            return EXIT_SUCCESS;
        draw ();
    }

    if ( check_same () )
        printf("\n\n\t\t     YOU  WIN   :)\n");
    else
        printf("\n\n\tYOU LOST, THE SUDOKU IS NOT GOOD :(\n");

    printf("\n\n");

    return EXIT_SUCCESS;
}
