#ifndef _MENU_H_
#define _MENU_H_

#define FINAL 1
#define CONTINUE 0

void save();

// Get a number and put it
void where_to_put () {

    int row, colum, num;

    printf("\n\tWhere do you want to put the number?: ");
    scanf(" %i %i", &row, &colum);

    if ( row < 0 || row > DIM || colum < 0 || colum > DIM ) {
        printf("\n\tThis row or colum is invalid.\n");
        where_to_put ();
    }
    else if ( board[row][colum] != 0 ) {
        printf("\n\tThis posicion is caught.\n");
        where_to_put ();
    }

    printf("\tWhat number do you want to put?: ");
    scanf(" %i", &num);

    if ( num < 0 || num > 9 ) {
        printf("\n\tThis number is invalid.\n");
        where_to_put ();
    }

    board[row][colum] = num;
}

// Get a number and delete it
void where_to_delete () {

    int row, colum, num;

    printf("\n\tWhere do you want to delete the number?: ");
    scanf(" %i %i", &row, &colum);

    if ( row < 0 || row > DIM || colum < 0 || colum > DIM ) {
        printf("\n\tThis row or colum is invalid.\n");
        where_to_delete ();
    }
    else if ( board[row][colum] == 0 ) {
        printf("\n\tThis posicion is already empty.\n");
        where_to_delete ();
    }

    board[row][colum] = 0;
}

// Show the menu
int menu () {

    int num;
    printf("\n\tWhat do you want to do?: \n\n"
            "\t  1.- Put number\n"
            "\t  2.- Delete number\n"
            "\t  3.- Leave and save\n"
            "\t  4.- Leave whithout save\n"
            "\n\tWrite the number: ");
    scanf(" %i", &num);

    switch(num) {
        case 1:
            where_to_put ();
            break;
        case 2:
            where_to_delete ();
            break;
        case 3:
            save ();
            return FINAL;
            break;
        case 4:
            return FINAL;
            break;
        default:
            draw ();
            printf("\n\tThis option is invalid.\n");
            menu ();
            break;
    }
    return CONTINUE;
}

#endif
