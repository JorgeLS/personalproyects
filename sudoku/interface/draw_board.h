#ifndef _DRAW_BOARD_H_
#define _DRAW_BOARD_H_

// Draw the title
void titulo() {

    system("clear");
    printf("\n");
    system("toilet -fpagga -Fborder '       SUDOKU        '");
    printf("\n\n");
}

// Draw the board
void draw () {

    titulo();
    printf("\t");
    for(int i=0; i<DIM; i++)
        printf("  %s%i ", ( i % 3 == 0 && i != 0 ) ? " " : "", i);
    printf("\n");
    for(int i = 0; i<=DIM; i++){
        if(i % 3 == 0)
            printf("\t―――――――――――――――――――――――――――――――――――――――\n");
        if(i == DIM)break;
        printf("     %i\t", i);
        for(int j=0; j<DIM; j++){
            if(board[i][j] == 0)
                printf("|   %s", j % 3 != 2 ? "" : "|");
            else
                printf("| %i %s", board[i][j], j % 3 != 2 ? "" : "|");
        }
        if(i % 3 != 2)
            printf("\n\t|---|---|---||---|---|---||---|---|---|\n");
        else
            printf("\n");
    }
}

#endif
