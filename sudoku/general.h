#ifndef _GENERAL_H_
#define _GENERAL_H_

// Defining the global variables
#define DIM 9
#define FILENAME "saves.dat"

int board[DIM][DIM];
int initial_board[DIM][DIM];

// Including the interface
#include "interface/draw_board.h"
#include "interface/menu.h"

// Including the functions
#include "functions/put_level.h"
#include "functions/put_numbers.h"
#include "functions/check_board.h"
#include "functions/matrix_to_resolve.h"
#include "functions/save_game.h"

#endif
