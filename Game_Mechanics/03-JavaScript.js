var demo
var width = 140
var height = 140
var DIM_board = 7
var DIM_door = 20
var dir //direction
var PosX
var PosY
var fillPos
var directions = [
	// Arriba
	[ (width-DIM_door)/2, 0 ],
	// Abajo
	[ (width-DIM_door)/2, (height-DIM_door)],
	// Derecha
	[ 0,				  (height-DIM_door)/2],
	// Izquierda
	[ (width-DIM_door),   (height-DIM_door)/2]
]

function creaTablero () {

	var tablero = document.getElementById("board")
	tablero.innerHTML = ""

	for (var i = 0; i < DIM_board; i++) {
		for (var j = 0; j < DIM_board; j++)
			tablero.innerHTML += "<canvas id='"+ i +" "+ j +"' width='"+ width +"px' height='"+ height +"px'></canvas>  "
		tablero.innerHTML += "<br>" 
	}

	creaE_S()
}

function creaE_S () {

	var canvas
	var end = false
	var n = 0
	fillPos = new Array()
	PosX = Math.round(DIM_board/2)
	PosY = Math.round(DIM_board/2)

	do {
		dir =  Math.floor(Math.random() * directions.length);
		canvas = document.getElementById(PosX +" "+ PosY).getContext("2d")
		dibujaE_S(canvas)

		// Muestra las posiciones
		demo.innerHTML += PosX + " - " + PosY + "<br>"
		fillPos.push([PosX, PosY])

		switch (dir) {
			case 0:
				PosX--
				dir = 1
				break;

			case 1:
				PosX++
				dir = 0
				break;

			case 2:
				PosY--
				dir = 3
				break;

			case 3:
				PosY++
				dir = 2
				break;
		}

		if ( (PosX < 0 || PosX >= DIM_board) || (PosY < 0 || PosY >= DIM_board) || n == 20 )
			end = true

		if (!end) {
			canvas = document.getElementById(PosX +" "+ PosY).getContext("2d")
			dibujaE_S(canvas)
		}

		n++
	}while(!end);

	demo.innerHTML += "<br><br>" + fillPos

	muestraMapa()
}

function muestraMapa () {

	for (var i = 0; i < fillPos.length; i++) {
	    document.getElementById(fillPos[i][0] +" "+ fillPos[i][1]).style.backgroundColor = "gray"
	}
}

function dibujaE_S (can) {

	can.beginPath()
	can.fillStyle = "red"
	can.fillRect(directions[dir][0], directions[dir][1], DIM_door, DIM_door)
	can.stroke()
}

function main () {

	document.body.innerHTML += "<p id=\"demo\" " +
		"style=\" " +
			"position: absolute;" +
			"top: 50px;" +
			"left: "+ (width * DIM_board + width) +"px; " +
		"\">"

	demo = document.getElementById('demo')

	creaTablero()
}