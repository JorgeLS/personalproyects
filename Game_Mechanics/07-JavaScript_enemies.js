var posEnemiesS = [
	[sizeObs * 25, sizeObs * 5],
	[sizeObs * 9, sizeObs * 25]
]
var dirEnemiesS = [
	[0, 1],
	[1, 0]
]

var posEnemiesC = [
	[sizeObs * 15, sizeObs * 5],
	[sizeObs * 5, sizeObs * 15],
	[sizeObs * 5, sizeObs * 10]
]
var dirEnemiesC = [
	[-1, 0],
	[0, -1],
	[1, 1]
]

// Square Enemies
function moveEnemiesS () {
	// Moves
	for (var i = 0; i < posEnemiesS.length; i++) {
		posEnemiesS[i][0] += dirEnemiesS[i][0]
		posEnemiesS[i][1] += dirEnemiesS[i][1]
	}

	// Hit with the walls
	for (var i = 0; i < posEnemiesS.length; i++) {
		// Hit with the right and left walls
		if (posEnemiesS[i][0] < sizeObs || posEnemiesS[i][0] + sizeEne > DIM[0] - sizeObs)
			dirEnemiesS[i][0] = -dirEnemiesS[i][0]
		// Hit with the up and down walls
		if (posEnemiesS[i][1] < sizeObs || posEnemiesS[i][1] + sizeEne > DIM[1] - sizeObs)
			dirEnemiesS[i][1] = -dirEnemiesS[i][1]
	}

	if ( hitCHA_ENE (posEnemiesS, sizeEne) )
		window.location.reload ()
}

// Circle Enemies
function moveEnemiesC () {
	// Moves
	for (var i = 0; i < posEnemiesC.length; i++) {
		posEnemiesC[i][0] += dirEnemiesC[i][0]
		posEnemiesC[i][1] += dirEnemiesC[i][1]
	}

	// Hit with the walls
	for (var i = 0; i < posEnemiesC.length; i++) {
		// Hit with the right and left walls
		if (posEnemiesC[i][0] - sizeEne / 2 < sizeObs || posEnemiesC[i][0] + sizeEne / 2 > DIM[0] - sizeObs)
			dirEnemiesC[i][0] = -dirEnemiesC[i][0]
		// Hit with the up and down walls
		if (posEnemiesC[i][1] - sizeEne / 2 < sizeObs || posEnemiesC[i][1] + sizeEne / 2 > DIM[1] - sizeObs)
			dirEnemiesC[i][1] = -dirEnemiesC[i][1]
	}
	/*
	if ( hitCHA_ENE (posEnemiesC, sizeEne/2) )
		window.location.reload ()*/
}