function setSpeed () {
	speed = $('#speed').val()
	getID('num_speed').innerHTML = speed + " m/s"
	draw ()
}

function setDegrees () {
	degree = $('#degrees').val()
	getID('num_degree').innerHTML = degree + " º"
	draw ()
}


function getX () {
	return cosine (degree) * speed
}
function getXtime (time) {
	return cosine (degree) * speed * time 
}

function getY () {
	return sine (degree) * speed
}
function getYtime (time) {
	return ( sine (degree) * speed * time ) - ( gravity * Math.pow(time, 2) / 2 )
}

function setNewPosition () {
	animeTime += speed / 1000
	pos[X] = getXtime (animeTime)
	pos[Y] = - getYtime (animeTime)
	if (animeTime > getTimeT()) {
		pos[X] = 0
		pos[Y] = 0
		animation = false
		animeTime = 0
	}
}

function getAltitude () {
	return ( Math.pow(speed, 2) * Math.pow( sine (degree), 2) ) / ( 2 * gravity )
}

function getRange () {
	return ( Math.pow(speed, 2) * sine (degree * 2) ) / gravity
}

function getTimeT () {
	return ( 2 * speed * sine (degree) ) / gravity
}

function getTimeH () {
	return ( speed * sine (degree) ) / gravity
}


function getColor () {
	return "rgb(" + (speed * 4) + ", " + ((100 - speed) * 4) + ", 0)" // The perfect symmetry would be 5.1 but I put 4
}

function getWidthLine () {
	return (speed * 6) / 100 + 1	// min = 1  max = 7
}