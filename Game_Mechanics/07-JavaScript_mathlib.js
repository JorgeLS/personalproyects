// Math fucntions
function hypotenuse (x, y) {
	var hypo = Math.sqrt( Math.pow(x, 2) + Math.pow(y, 2))
	
	return hypo
}

function gradient (x1, y1, x2, y2) {
	var m = (y2 - y1) / (x2 - x1)

	return m
}

function makeVector (point1, point2) {
	return [ point2[0] - point1[0], point2[1] - point1[1] ]
}

function angleTwoVectors (initialPoint, point1, point2) {
	var vector1 = makeVector (initialPoint, point1)
	var vector2 = makeVector (initialPoint, point2)
	var module1 = moduleFun (vector1)
	var module2 = moduleFun (vector2)

	return Math.acos( dotProduct(vector1, vector2) / (module1 * module2) ) * 180 / Math.PI
}

function getAngleSC (point1, inc1, point2, inc2) {

	return angleTwoVectors (
		[point1[0] + inc1    , point1[1] + inc1],
		[point1[0] + inc1 * 2, point1[1] + inc1],
		[point2[0] + inc2    , point2[1] + inc2] )
}

function moduleFun (vector) {
	return Math.sqrt( Math.pow(vector[0], 2) + Math.pow(vector[1], 2) )
}

function dotProduct (vector1, vector2) {
	return vector1[0] * vector2[0] + vector1[1] * vector2[1]
}

function getRadioSquare (angle, sizeS) {
	if (angle > 90)
		angle = 180 - angle

	if (angle == 0)
		return 20

	if (angle < 45)
		return ( sizeS / Math.cos(angle * Math.PI / 180) )
	else
		return ( sizeS / Math.sin(angle * Math.PI / 180) )
}