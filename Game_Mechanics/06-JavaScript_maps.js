// Function to create the map
function createMap () {

	if (xIni == 0 && yIni == 0) {

		maps[0]()
		mapR = 0
		resetBoard ()
		posX = Math.floor(colum / 2)
		posY = Math.floor(row / 2)
	}
	else {
		var map
		do {
			map = Math.floor (Math.random() * maps.length)
		} while ( !maps[map]() || map == mapR)

		mapR = map
		resetBoard ()

		posX = xIni
		posY = yIni
	}

	for (var i = 0; i < row; i++) {
		for (var j = 0; j < colum; j++) {
			// Border
			if (i == 0 || i == row-1 || j == 0 || j == colum-1)
				bitMap[i][j] = 1
			else
				bitMap[i][j] = 0
		}
	}

	for (var i = 0; i < posObstacles.length; i++)
		bitMap[ posObstacles[i][0] ][ posObstacles[i][1] ] = 3


	for (var i = 0; i < doors.length; i++)
		bitMap[ posDoor[ doors[i] ][0] ][ posDoor[ doors[i] ][1] ] = 2

	paintHero (0, 0)
}

// map1 no have any obstacles and w = 800 & h = 800
// Initial map
function map0 () {

	// W 750 - H 750
	switch (key) {
		case "ArrowUp":
		case "ArrowDown":
			if (width == 750)
				height = 750
			else
				return false
			break
		case "ArrowRight":
		case "ArrowLeft":
			if (height == 750)
				width = 750
			else
				return false
			break
	}

	// In this map the hero can enter anywhere
	doors = [0, 1, 2, 3]
	posObstacles = []

	return true
}

function map1 () {

	// W 750 - H 650 
	switch (key) {
		case "ArrowDown":
		case "ArrowRight":
			return false
			break
		case "ArrowUp":
			if (width == 750)
				height = 650
			else
				return false
			break
		case "ArrowLeft":
			if (height == 650)
				width = 750
			else
				return false
			break
	}

	doors = [1, 3]
	posObstacles = [
		[5, 5],
		[5, 6],
		[5, 7]
	]
	
	return true
}

function map2 () {

	// W 650 - H 750
	switch (key) {
		case "ArrowDown":
		case "ArrowRight":
			return false
			break
		case "ArrowUp":
			if (width == 650)
				height = 750
			else
				return false
			break
		case "ArrowLeft":
			if (height == 750)
				width = 650
			else
				return false
			break
	}

	doors = [1, 3]
	posObstacles = [
		[5, 5], [5, 6], [5, 7],
		[7, 8],
		[9, 6], [9, 7], [9, 6]
	]
	
	return true
}

function map3 () {

	// W 550 - H 850
	switch (key) {
		case "ArrowDown":
		case "ArrowLeft":
		case "ArrowRight":
			return false
			break
		case "ArrowUp":
			if (width == 550)
				height = 850
			else
				return false
			break
	}

	doors = [1]
	posObstacles = []
	
	return true
}

function map4 () {

	// W 750 -- H 750
	switch (key) {
		case "ArrowUp":
		case "ArrowLeft":
			return false
			break
		case "ArrowDown":
			if (width == 750)
				height = 750
			else
				return false
			break
		case "ArrowRight":
			if (height == 750)
				width = 750
			else
				return false
			break
	}

	doors = [0, 2]
	posObstacles = [
		[ 1, 2], [ 2, 2], [ 3, 2], [ 4, 2], [ 5, 2], 
		[ 6, 1],
		[ 8, 1],
		[ 9, 2], [10, 2], [11, 2], [12, 2], [13, 2]
	]
	
	return true
}

function map5 () {

	// W 650 -- H 650
	switch (key) {
		case "ArrowUp":
		case "ArrowLeft":
			return false
			break
		case "ArrowDown":
			if (width == 650)
				height = 650
			else
				return false
			break
		case "ArrowRight":
			if (height == 650)
				width = 650
			else
				return false
			break
	}

	doors = [0, 2]
	posObstacles = []

	return true
}

function map6 () {

	// W 550 -- H 650
	switch (key) {
		case "ArrowUp":
		case "ArrowRight":
			return false
			break
		case "ArrowDown":
			if (width == 550)
				height = 650
			else
				return false
		case "ArrowLeft":
			if (height == 650)
				width = 550
			else
				return false
			break
	}

	doors = [0, 3]
	posObstacles = []

	return true
}