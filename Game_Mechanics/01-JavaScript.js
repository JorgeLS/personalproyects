
var lienzo
var lienzo2
var nada = "white"
//var grass = new Array("green", "light green")
var grass = new Array ("#00CC00", "#00BB00")
var rock = "#666666"
var way = new Array ("#663300", "#996600")
var water = "cyan"
var personaje = new Array ("#")
var tablero
var DIM = 20
var movimientos = new Array(0, 0, 1, 1, 1)
var moving = new Array([0, 0, -1, 1])
var pos = new Array([0, 10], [0, 10], [10, 0], [19, 10])
var move = new Array([1, 0], [1, 1], [-1, 1], [-1, 0]) //Primer canvas Segundo Canvas Tercer Canvas Cuarto Canvas
var fila
var columna
var canvas = 1
var PosX = 1
var PosY = 1

var demo

function dibujaTablero(num) {

	lienzo.beginPath()
	for(var i = 0; i < DIM; i++) {
		for(var j = 0; j < DIM; j++) {
			lienzo.fillStyle = tablero[num][i][j]
			lienzo.fillRect(20*i, 20*j, 20, 20)
		}
	}
	lienzo.stroke()
}

function copiarTablero(num) {

	lienzo2.beginPath()
	for(var i = 0; i < DIM; i++) {
		for(var j = 0; j < DIM; j++) {
			lienzo2.fillStyle = tablero[num][i][j]
			lienzo2.fillRect(20*i, 20*j, 20, 20)
		}
	}
	lienzo2.stroke()
}

function creaCampo(num) {

	var x
	var y

	// Bzero tablero
	tablero[num] = new Array()

	for (var i = 0; i < DIM; i++) {

		tablero[num][i] = new Array()

		for (var j = 0; j < DIM; j++)
			tablero[num][i].unshift(nada)
	}

	// Rocks
	for (var j = 0; j < 20; j++) {

		x = Math.floor(Math.random() * DIM);
			y = Math.floor(Math.random() * DIM);

			tablero[num][x][y] = rock
		}

	// More Rocks
	for (var i = 0; i < DIM; i++) {
		for (var j = 0; j < DIM; j++) {
			if(tablero[num][i][j] != rock)
				tablero[num][i][j] = grass[Math.floor(Math.random() * grass.length)]
			else {
				for(var k = 0; k < Math.floor(Math.random() * 4); k++) {
					x = movimientos[ Math.floor(Math.random() * movimientos.length) ]
					y = movimientos[ Math.floor(Math.random() * movimientos.length) ]
					
					if(i+x >= 0 && i+x < DIM && j+y >= 0 && j+y < DIM)
						tablero[num][i+x][j+y] = rock
				}
			}
		}
	}

	// Way
	fila = pos[num][0]
	columna = pos[num][1]

	do {
		tablero[num][fila][columna] = way[ Math.floor(Math.random() * way.length) ]

		x = movimientos[ Math.floor(Math.random() * movimientos.length) ]
		y = movimientos[ Math.floor(Math.random() * movimientos.length) ]

		if (num == 0) {
			fila += move[num][0]
			columna += move[num][1] + moving[0][ Math.floor(Math.random() * moving[0].length) ]
		}
		else if (num == 1) {
			fila += move[num][0]
			columna += move[num][1]
		}
		else if (num == 2) {
			fila += moving[0][ Math.floor(Math.random() * moving[0].length) ] 
			columna += move[num][1]
		}
		else {
			fila += move[num][0]
			columna += move[num][1] + moving[0][ Math.floor(Math.random() * moving[0].length) ]
		}

		// Up    --> Down  fila   columna++
		// Up    --> Right fila++ columna++
		// Up    --> Left  fila-- columna++
		// Down  --> Up    fila   columna--
		// Down  --> Right fila++ columna--
		// Down  --> Left  fila-- columna--
		// Right --> Left  fila-- columna
		// Right --> Down  fila-- columna++
		// Right --> Up    fila-- columna--
		// Left  --> Right fila++ columna
		// Left  --> Down  fila++ columna++
		// Left  --> Up    fila++ columna--

		if((fila < 0 || fila >= DIM) || (columna < 0 || columna >= DIM))
			break
	} while(1);

	if (num == 0)
		pos[1][1] = columna
	else if (num == 1)
		pos[2][0] = fila
	else if (num == 2)
		pos[3][1] = columna

	// lake
	if (num == 3) {

		x = Math.floor(Math.random() * 5) + 9
		y = Math.floor(Math.random() * 5) + 9

		tablero[num][x][y] = water

		for (var i = 0; i < 15; i++) {
			var x2 = x + moving[0][ Math.floor(Math.random() * moving[0].length) ]
			var y2 = y + moving[0][ Math.floor(Math.random() * moving[0].length) ] 
			tablero[num][x2][y2] = water
			if(tablero[num][x2][y2] == water) {
				x2 += Math.floor(Math.random() * moving[0].length)
				y2 += Math.floor(Math.random() * moving[0].length)
				tablero[num][x2][y2] = water
			}

		}
	}
}

function dibujaPersona () {

	lienzo = document.getElementsByClassName("t"+canvas)[0].getContext("2d")


	if (tablero[canvas-1][PosX][PosY] != water) {
		lienzo.beginPath()
		lienzo.fillStyle = "blue"
		lienzo.fillRect(20*PosX, 20*PosY, 20, 20)
		lienzo.stroke()
		lienzo2.beginPath()
		lienzo2.fillStyle = "blue"
		lienzo2.fillRect(20*PosX, 20*PosY, 20, 20)
		lienzo2.stroke()
	}
	else {
		lienzo.beginPath()
		lienzo.fillStyle = "red"
		lienzo.fillRect(20*PosX, 20*PosY, 20, 20)
		lienzo.stroke()
		lienzo2.beginPath()
		lienzo2.fillStyle = "red"
		lienzo2.fillRect(20*PosX, 20*PosY, 20, 20)
		lienzo2.stroke()					
	}
}

function borraPersona () {

	lienzo = document.getElementsByClassName("t"+canvas)[0].getContext("2d")

	lienzo.beginPath()
	lienzo.fillStyle = tablero[canvas-1][PosX][PosY]
	lienzo.fillRect(20*PosX, 20*PosY, 20, 20)
	lienzo.stroke()

	lienzo2.beginPath()
	lienzo2.fillStyle = tablero[canvas-1][PosX][PosY]
	lienzo2.fillRect(20*PosX, 20*PosY, 20, 20)
	lienzo2.stroke()
}

function getPos(event) {		

	var key = event.key
	var can = canvas-1
	var dim = DIM-1

	borraPersona()
	
	if (key == "ArrowUp") {
		if (canvas == 3 && PosY == 0 && tablero[1][PosX][dim] != rock) {
			canvas = 2
			PosY = dim
		}
		else if (canvas == 4 && PosY == 0 && tablero[0][PosX][dim] != rock) {
			canvas = 1
			PosY = dim
		}

		else if (PosY > 0 && tablero[can][PosX][PosY-1] != rock)
			PosY--
	}

	if (key == "ArrowDown") {
		if (canvas == 1 && PosY == DIM-1 && tablero[3][PosX][0] != rock) {
			canvas = 4
			PosY = 0
		}
		else if (canvas == 2 && PosY == DIM-1 && tablero[2][PosX][0] != rock) {
			canvas = 3
			PosY = 0
		}
		else if (PosY < DIM && tablero[can][PosX][PosY+1] != rock)
			PosY++
	}

	if (key == "ArrowLeft") {
		if (canvas == 2 && PosX == 0 && tablero[0][dim][PosY] != rock) {
			canvas = 1
			PosX = dim
		}
		else if (canvas == 3 && PosX == 0 && tablero[3][dim][PosY] != rock) {
			canvas = 4
			PosX = dim
		}
		else if (PosX > 0 && tablero[can][PosX-1][PosY] != rock)
			PosX--
	}

	if (key == "ArrowRight") {
		if(canvas == 1 && PosX == DIM-1 && tablero[1][0][PosY] != rock) {
			canvas = 2
			PosX = 0
		}
		else if(canvas == 4 && PosX == DIM-1 && tablero[2][0][PosY] != rock) {
			canvas = 3
			PosX = 0
		}
		else if (PosX < DIM && tablero[can][PosX+1][PosY] != rock)
			PosX++
	}

	demo.innerHTML = PosX + " - " + PosY

	copiarTablero(canvas-1)
	dibujaPersona()
}

function main() {

	demo = document.getElementById('demo')

	lienzo2 = document.getElementsByClassName("t5")[0].getContext("2d")

	tablero = new Array()

	for(var i = 1; i < 5; i++) {
		lienzo = document.getElementsByClassName("t"+i)[0].getContext("2d")
		creaCampo(i-1)
		dibujaTablero(i-1)
	}

	copiarTablero(0);
}