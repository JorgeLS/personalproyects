// Global Variables
var canvas
var DIM
var size = 40
var sizeObs = 20
var sizeEne = 30

var pos = new Array(2)
var posX 
var posY

var posObstacles = [
	[sizeObs * 14, sizeObs * 13],
	[sizeObs * 13, sizeObs * 14],
	[sizeObs * 5, sizeObs * 26],
	[sizeObs * 6, sizeObs * 26]
]

var vel = 1
var n = 0
var stop = false

var left = false, right = false, up = false, down = false

var directions = ["left", "right", "up", "down"]

setInterval(
	function() {
		posX = pos[0]
		posY = pos[1]
		if (!stop) {
			moveHero ()
			moveEnemiesS ()
			moveEnemiesC  ()
		}
		paint ()
	},
	10)

window.addEventListener('keydown', function(event) {
	if (event.key == "p")
		stop = !stop
})

function paint () {

	canvas.clearRect(0, 0, DIM[0], DIM[1])

	// Tablero
	canvas.beginPath()
	for (var i = 0; i < DIM[0]; i += sizeObs) {
		for (var j = 0; j < DIM[1]; j += sizeObs) {
		
		if (i == 0 || j == 0 ||  i == DIM[0] - sizeObs || j == DIM[1] - sizeObs)
			canvas.fillStyle = "black"
		else if ( paintObstacles (i, j) )
			canvas.fillStyle = "green"
		else
			canvas.fillStyle = "white"

		canvas.fillRect(i, j, sizeObs, sizeObs)
		}
	}
	canvas.stroke()

	// Character
	canvas.beginPath()
	canvas.fillStyle = "red"
	//canvas.arc(posX, posY, size/2, 0, 2 * Math.PI)
	canvas.rect(posX, posY, size, size)
	canvas.fill()

	// Square Enemies
	for (var i = 0; i < posEnemiesS.length; i++) {
		canvas.beginPath()
		canvas.fillStyle = "yellow"
		canvas.rect(posEnemiesS[i][0], posEnemiesS[i][1], sizeEne, sizeEne)
		canvas.fill()
	}

	// Circle Enemies
	for (var i = 0; i < posEnemiesC.length; i++) {
		canvas.beginPath()
		canvas.fillStyle = "lime"
		canvas.arc(posEnemiesC[i][0], posEnemiesC[i][1], sizeEne / 2, 0, 2 * Math.PI)
		canvas.fill()
	}

	canvas.beginPath()
	canvas.moveTo(pos[0] + size/2, pos[1] + size/2)
	canvas.lineTo(posEnemiesC[2][0], posEnemiesC[2][1])
	canvas.stroke()

	showVariables ()

	if ( hitCC (posEnemiesC[0], sizeEne/2, posEnemiesC[1], sizeEne/2) )
		getID('demo').innerHTML += "" +
			"Choque 1 = true<br>"
	if ( hitSC (pos, size/2, posEnemiesC[2], sizeEne/2) )
		getID('demo').innerHTML += "" +
			"Choque 2 = true<br>"
	if ( hitSS (posEnemiesS[0], sizeEne/2, posEnemiesS[1], sizeEne/2) ) {
		getID('demo').innerHTML += "" +
			"Choque 3 = true<br>"
		// Derecha 0
		if (posEnemiesS[0][0] > posEnemiesS[1][0]) {
			dirEnemiesS[0][0] = -dirEnemiesS[0][0]
			dirEnemiesS[0][1] = -dirEnemiesS[0][1]
		}
		// Derecha 1
		else {
			dirEnemiesS[1][0] = -dirEnemiesS[1][0]
			dirEnemiesS[1][1] = -dirEnemiesS[1][1]
		}
	}

	canvas.beginPath()
	canvas.arc(pos[0] + size/2, pos[1] + size/2,
		getRadioSquare (getAngleSC (pos, size/2, posEnemiesC[2], 0), size/2),
		0, 2 * Math.PI)
	canvas.stroke()
}

function paintObstacles (x, y) {
	for (var i = 0; i < posObstacles.length; i++) {
		if (posObstacles[i][0] == x && posObstacles[i][1] == y)
			return true
	}
	return false
}

function events () {
	document.addEventListener("keydown",
		function () {
			var key = event.key
			getKey(key, true)
		}
	)
	document.addEventListener("keyup",
		function () {
			var key = event.key
			getKey(key, false)
		}
	)
}

function main () {
	
	canvas = getID('board').getContext('2d')
	DIM = [getID('board').width, getID('board').height]
	
	events ()

	pos = [ DIM[0] / 2, DIM[1] / 2 ]
}

// Shortener functions
function getID (id) {
	return document.getElementById(id)
}

// Show function
function showVariables () {

	getID('demo').innerHTML = "" +
		"rad MAX = " + hypotenuse(size/2, size/2) + "<br>" +
		"rad MIN = " + hypotenuse(size/2, 0) + "<br>" +
		"rad Aprox1 = " + hypotenuse(400-(pos[0] + size / 2), 200-(pos[1] + size / 2)) + "<br>" +
		"r1 = " + (sizeEne / 2) + "<br>" +
		"r2 = " + (sizeEne / 2) + "<br>" +
		"r3 = " + (sizeEne / 2) + "<br>" +
		"r1 + r2 || r1 + r3 || r2 + r3 = " + sizeEne + "<br>" +
		"distancia 0 to 1 = " + hitCC(posEnemiesC[0], size/2, posEnemiesC[1], size/2) + "<br>" +
		"distancia 2 to Char = " + hitSC( (pos[0] + size/2), (pos[1] + size/2), posEnemiesC[2] ) + "<br>" +
		"angulo Char to 2C = " + getAngleSC (pos, size/2, posEnemiesC[2], 0).toFixed(2) + "<br>" +
		"radio Char to 2C = " + getRadioSquare (getAngleSC (pos, size/2, posEnemiesC[2], 0), size/2).toFixed(2) + "<br>"
}