
var minimap // largo = 300 - altura = 150
var game // largo = 300 - altura = 150
var fondo
var demo
var PosX_map
var PosX_game
var PosY_map
var PosY_game
var DIM = 30
var tablero = new Array()
var white = 0
var black = 1
var color = 0

function creaMapa() {

	// floor
	minimap.beginPath()
	minimap.fillStyle = "green"
	minimap.fillRect(0, 125, 300, 5)
	minimap.fillStyle = "brown"
	minimap.fillRect(0, 130, 300, 20)
	minimap.stroke()

	// freak cloud			
	for (var i = 0; i < 30; i++) {
		if(i % 2 == 0) {
			minimap.beginPath()
			minimap.fillStyle = "white"
			minimap.fillRect(i*10, 0, 10, 10)
			minimap.stroke()

			tablero.unshift(white);
		}
		else {
			minimap.beginPath()
			minimap.fillStyle = "black"
			minimap.fillRect(i*10, 0, 10, 10)
			minimap.stroke()

			tablero.unshift(black)
		}
	}
}

function creaGame() {

	// floor
	game.beginPath()
	game.fillStyle = "green"
	game.fillRect(0, 125, 300, 5)
	game.fillStyle = "brown"
	game.fillRect(0, 130, 300, 20)
	game.stroke()

	// freak cloud
	for (var i = 0; i < 15; i++) {
		if(i % 2 == color) {
			game.beginPath()
			game.fillStyle = "white"
			game.fillRect(i*20, 0, 20, 10)
			game.stroke()
		}
		else {
			game.beginPath()
			game.fillStyle = "black"
			game.fillRect(i*20, 0, 20, 10)
			game.stroke()
		}
	}
}

function creaPersonajeMapa() {

	minimap.beginPath()
	minimap.fillStyle = "gray"
	minimap.fillRect(PosX_map*10, 110 + PosY_map, 10, 15)
	minimap.stroke()
}

function creaPersonajeGame() {

	game.beginPath()
	game.fillStyle = "gray"
	game.fillRect(PosX_game*20, 110 + PosY_game, 20, 15)
	game.stroke()

}

function borraPersonaje() {

	minimap.beginPath()
	minimap.fillStyle = "cyan"
	minimap.fillRect(PosX_map*10, 110 + PosY_map, 10, 15)
	minimap.stroke()

	game.beginPath()
	game.fillStyle = "cyan"
	game.fillRect(PosX_game*20, 110 + PosY_game, 20, 15)
	game.stroke()
}

function muevePersonaje(event) {
	var key = event.key

	borraPersonaje()

	if(key == "ArrowRight" && PosX_map+1 != DIM) {
		PosX_map++
	}
	if(key == "ArrowLeft" && PosX_map != 0) {
		PosX_map--
	}
	if(key == "ArrowUp") {
		for (var i = 1; i < 15; i++)
			salto(0, 1)
		demo.innerHTML = PosY_game + "  --  "
		for (var i = 1; i < 15; i++)
			setTimeout(function() {salto(1, 1)}, 150)
		demo.innerHTML += PosY_game + "<br>"

	}


	if (PosX_map > 7 && PosX_map < 23)
		scroll(key)
	else {
		if(key == "ArrowRight" && PosX_game+1 != DIM/2)
			PosX_game++
		if(key == "ArrowLeft" && PosX_game != 0)
			PosX_game--
	}

	creaPersonajeMapa()
	creaPersonajeGame()
}

function scroll(key) {

	if (key == "ArrowRight") {
		color = (color+1) % 2
	}
	if (key == "ArrowLeft") {
		color = (color+1) % 2
	}

	creaGame()
}

function salto(x, num) {
	if (x == 0) {
		borraPersonaje()
		PosY_map -= num
		PosY_game -= num
	}
	else if (x == 1) {
		borraPersonaje()
		PosY_map += num
		PosY_game += num
	}

	creaPersonajeMapa()
	creaPersonajeGame()
}

function main() {

	minimap = document.getElementById('minimap').getContext("2d")
	game = document.getElementById('board').getContext("2d")

	PosX_map = 0
	PosX_game = 0
	PosY_map = 0
	PosY_game = 0

	creaMapa()
	creaGame()
	creaPersonajeMapa()
	creaPersonajeGame()

	demo = document.getElementById('demo')
}
