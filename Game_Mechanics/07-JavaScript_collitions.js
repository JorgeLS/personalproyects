var collitions = [
	// Right
	[0,	0, 1, 1],
	// Left
	[0, 1, 0, 1],
	// Up
	[1,	0, 1, 0],
	// Bottom
	[1, 1, 0, 0]
]

// Collitions between Character and Obstacles
function hitCHA_OBJ (first, sum1, sum2, second) {
	// first = 1
	// sum1 = 0
	// sum2 = 1
	// second = 0
	for (var obs = 0; obs < posObstacles.length; obs++) {
		for (var i = 0; i < size; i++) {
			if (
				pos[first] + sum1 == posObstacles[obs][first] + sum2
				&&
				(pos[second] + i >= posObstacles[obs][second] && pos[second] + i <= posObstacles[obs][second] + sizeObs)
			)
				return true
		}
	}
	return false
}

// Collitions between Character and enemies
function hitCHA_ENE (object, sizeObject) {

	for (var i = 0; i < object.length; i++) {

		for (var j = 0; j < collitions.length; j++) {

			var sizeHero = collitions[j][1] * size
			var sizeObj = collitions[j][2] * sizeObject

			for (var k = 0; k < size; k++) {
				if (
					pos[ collitions[j][0] ] + sizeHero == object[i][ collitions[j][0] ] + sizeObj
					&&
					(
						pos[ collitions[j][3] ] + k >= object[i][ collitions[j][3] ] &&
						pos[ collitions[j][3] ] + k <= object[i][ collitions[j][3] ] + sizeObject
					)
				)
					return true
			}
		}
	}
	return false
}

// Function that takes the distance to know if they hit
// Circle to circle
function hitCC (obj1, radio1, obj2, radio2) {

	var dis = hypotenuse ( obj2[0] - obj1[0], obj2[1] - obj1[1] )
	return Math.round(dis) <= radio1 + radio2
}
// Square to circle
function hitSC (obj1, sizeS, obj2, radio) {

	var radioS = getRadioSquare (getAngleSC (obj1, sizeS, obj2, 0), sizeS)
	var dis = hypotenuse ( obj2[0] - (obj1[0] + sizeS), obj2[1] - (obj1[1] + sizeS) )

	return Math.round(dis) <=  radioS + radio
}
// Square to square
function hitSS (obj1, size1, obj2, size2) {

	var dis = hypotenuse ( (obj2[0] + size2) - (obj1[0] + size1), (obj2[1] + size2) - (obj1[1] + size1) )
	return Math.round(dis) <= size1 + size2
}