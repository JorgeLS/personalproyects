function draw () {
	canvas.clearRect(0, 0, width, height)
	drawCoords ()
	drawBall ()
}


function drawCoords () {

		// Coords
	canvas.beginPath()
	canvas.lineWidth = 1
	canvas.strokeStyle = "black"
	// X
	canvas.moveTo(0, Y0)
	canvas.lineTo(width, Y0)
	// Y
	canvas.moveTo(X0, 0)
	canvas.lineTo(X0, height)

	canvas.stroke()

		// Values
	// X
	for (var i = 0; i <= width; i+=50) {
		canvas.beginPath()
		canvas.moveTo(i, Y0-5)
		canvas.lineTo(i, Y0+5)
		canvas.font = "10px Arial"
		canvas.fillText(i-50, i+2, Y0+17)
		canvas.stroke()
	}
	// Y
	for (var i = Y0; i >= Y0-height; i-=50) {
		canvas.beginPath()
		canvas.moveTo(X0+5, Y0-i)
		canvas.lineTo(X0-5, Y0-i)
		canvas.font = "10px Arial";
		if(i != 0)
			canvas.fillText(i, X0-30, Y0-i);
		canvas.stroke()
	}
}

function drawBall () {
	
	if (!shootBool) {
		// Shoot Vector
		canvas.beginPath()
		canvas.lineWidth = getWidthLine ()
		canvas.strokeStyle = getColor ()
		canvas.moveTo(pos[X] + X0, pos[Y] + Y0)
		//  Angle
		canvas.lineTo(pos[X] + X0 + getX(), pos[Y] + Y0 - getY())
		canvas.stroke()
	}

	// Ball
	canvas.beginPath()
	canvas.fillStyle = "black"
	canvas.arc(pos[X] + X0, pos[Y] + Y0, 8, 0, 2 * Math.PI)
	canvas.fill()
}

function drawPath () {
	for (var i = 0; i < getTimeT(); i += 0.25) {
		canvas.beginPath()
		canvas.moveTo(X0 + getXtime(i), Y0 - getYtime(i))
		canvas.lineTo(X0 + getXtime(i+0.1), Y0 - getYtime(i+0.1))
		canvas.stroke()
	}
}