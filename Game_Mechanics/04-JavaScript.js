// JQuery
$(document).ready(paint)
$(document).ready(function () {
	$("#hit").click(function () { if (life > 0 ) hit (10) })
	$("#bigHit").click(function () { if (life > 0 ) hit (5) })
	$("#kill").click(kill)
	$("#restoreLive").click(restoreLive)
	$("#potion").click(potion)
})

// Global Variables
var canvas
var life = 1
var lifeAni = life
var hitAni

function paint () {
	canvas = document.getElementById('board').getContext('2d')
	canvas.clearRect(0, 0, 500, 200)
	
	canvas.beginPath()
	canvas.fillStyle = "black"
	canvas.rect(50, 75, 400, 50)
	canvas.fill()

	canvas.beginPath()
	canvas.fillStyle = "gray"
	canvas.rect(60, 80, 380, 40)
	canvas.fill()

	canvas.beginPath()
	canvas.strokeStyle = "lightgray"
	canvas.rect(50, 75, 400, 50)
	canvas.stroke()
	canvas.strokeStyle = "gray"
	canvas.rect(60, 80, 380, 40)
	canvas.stroke()

	canvas.beginPath()
	canvas.fillStyle = "white"
	canvas.rect(439, 81, -378 * lifeAni, 38)
	canvas.fill()

	canvas.beginPath()
	canvas.fillStyle = "red"
	canvas.rect(439, 81, -378 * life, 38)
	canvas.fill()

	canvas.beginPath()
	canvas.fillStyle = "black"
	canvas.font = "15px Arial black";
	canvas.fillText( (lifeAni * 100).toFixed(0), 410, 70); 
}

function hit (x) {
	lifeAni = life
	life -= Math.random() / x
	if (life <= 0)
		life = 0

	paint ()

	if (life == 0)
		alert('You are dead')

	setTimeout(function () {
		speedAni = Math.abs(lifeAni - life)
		hitAni = setInterval(setLifeBetter, 100)
	}, 100)
}
	
function restoreLive () {
	lifeAni = life
	if (life == 1)
		alert('You have all your life.')
	life = 1

	paint ()
}

function setLifeBetter () {

	if (life <= lifeAni)
		lifeAni -= speedAni / 10
	else
		clearInterval(hitAni)

	if (lifeAni <= 0) {
		lifeAni = 0
	}

	paint ()
}

function potion () {
	if (life == 1)
		alert('You can not use a potion.')

	life += 0.1
	if (life >= 1)
		life = 1

	lifeAni = life
	
	paint ()
}

function kill () {
	lifeAni = life
	if (life > 0) {
		life = 0
		setTimeout(function () {
			speedAni = 0.7
			hitAni = setInterval(setLifeBetter, 100)
		}, 100)
	}
}