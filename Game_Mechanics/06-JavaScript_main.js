var board

var width = 750
var height = 750
var dim = 50

var row
var colum
var key

// Positions of the hero
var posX, posY
var xIni = 0, yIni = 0

// Creating the array
var bitMap
// Position of the doors and obstacles
var posDoor
// 0-Top 1-buttom 2-left 3-right
var doors
var posObstacles

var maps = [
	function() { return map0() },
	function() { return map1() },
	function() { return map2() },
	function() { return map3() },
	function() { return map4() },
	function() { return map5() },
	function() { return map6() }
]
var mapR


function drawMap () {

	board.beginPath()
	board.strokeStyle = "#FF5555"
	for (var i = 0; i < row; i++) {
		for (var j = 0; j < colum; j++) {
			switch ( bitMap[i][j] ) {
				// Floor
				case 0:
					board.fillStyle = "#FFFFFF"
					break
				// Wall
				case 1:
					board.fillStyle = "#000000"
					break
				// Doors
				case 2:
					board.fillStyle = "#555555"
					break
				// Obstacles
				case 3:
					board.fillStyle = "#5555FF"
					break
				// Character
				default:
					board.fillStyle = "#FF3333"
					break
			}
			board.fillRect (j * dim, i * dim, dim, dim)
			board.strokeRect (j * dim, i * dim, dim, dim)
		}
	}
	board.stroke()
}

// Get the key that has been pressed
function getKey (event) {

	key = event.key

	switch (key) {
		case "ArrowUp":
			paintHero ( 0, -1)
			break
		case "ArrowDown":
			paintHero ( 0,  1)
			break
		case "ArrowLeft":
			paintHero (-1,  0)
			break
		case "ArrowRight":
			paintHero ( 1,  0)
			break	
	}
	demo.innerHTML = mapR + " - w: " + width + "  h: " + height 
}

// Paint the hero
function paintHero (x, y) {

	bitMap[posY][posX] = 0

	if (bitMap[posY + y][posX + x] == 0) {
		posX += x
		posY += y
	}
	else if (bitMap[posY + y][posX + x] == 2) {
		xIni = posX
		yIni = posY
		createMap ()
	}

	bitMap[posY][posX] = 4
	drawMap()
 }

// Reset some variables with the new width and height
function resetBoard () {

	board.clearRect ( 0, 0, 1000, 1000)

	row = height / dim
	colum = width / dim

	bitMap = new Array(row);
	for (var i = 0; i < row; i++) {
		bitMap[i] = new Array(colum)
	}

	posDoor = [
		// Top
		[					 0, Math.floor(colum / 2)],
		// Bottom
		[ 				row-1, 	Math.floor(colum / 2)],
		// Left
		[ Math.floor(row / 2),	0],
		// Right
		[ Math.floor(row / 2),	colum-1]
	]
	changePosIniMap()
}

// Change the initial position on the map
function changePosIniMap () {

	switch (key) {
		case "ArrowUp":
			xIni = posDoor[1][1]
			yIni = posDoor[1][0] - 1
			break
		case "ArrowDown":
			xIni = posDoor[0][1]
			yIni = posDoor[0][0] + 1
			break
		case "ArrowLeft":
			xIni = posDoor[3][1] - 1
			yIni = posDoor[3][0]
			break
		case "ArrowRight":
			xIni = posDoor[2][1] + 1
			yIni = posDoor[2][0]
			break
	}
}

// Main function who loaded when the page is loads
function main (event) {

	demo = document.getElementById('demo')
	board = document.getElementById('board').getContext('2d')

	width = 750
	height = 750
	resetBoard ()

	createMap ()
	
	window.onkeydown = function (e) { getKey (e) }
}