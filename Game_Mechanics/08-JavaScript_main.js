
var pos = [0, 0]

var X = 0, X0 = 50
var Y = 1, Y0 = 400
var gravity = 9.8

var degree
var speed

var shootBool = false
var animation = false
var animeTime

var width
var height

$(document).ready( function () {
	
	degree = $('#degrees').val()
	speed = $('#speed').val()
	width = $('#board').width()
	height = $('#board').height()

	canvas = getID('board').getContext('2d')

	events ()
})

setInterval ( function () {
	setSpeed ()
	setDegrees ()
	draw ()
	if (shootBool)
		drawPath ()
	if (animation)
		setNewPosition ()
}, 10)

function shoot () {
	putScore ()
	shootBool = true
	animation = true
	animeTime = 0
}

function putScore () {
	getID('altitude').innerHTML = getAltitude().toFixed(2) + " m"
	getID('range').innerHTML = getRange().toFixed(2) + " m"
	getID('timeT').innerHTML = getTimeT().toFixed(2) + " s"
	getID('timeH').innerHTML = getTimeH().toFixed(2) + " s"
}

function getID(id) {
	return document.getElementById(id) 
}

function sine (angle) {
	return Math.sin(angle * Math.PI / 180)
}

function cosine (angle) {
	return Math.cos(angle * Math.PI / 180)
}


function events () {
	getID('speed').addEventListener("change", setSpeed)
	getID('degrees').addEventListener("change", setDegrees)
	getID('shoot').addEventListener("click", shoot)
}