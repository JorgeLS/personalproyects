function getKey (key, bool) {

	switch (key) {
		case "ArrowLeft":
		case "a":
			left = bool
			break
		case "ArrowRight":
		case "d":
			right = bool
			break
		case "ArrowUp":
		case "w":
			up = bool
			break
		case "ArrowDown":
		case "s":
			down = bool
			break
	}
}

function moveHero () {

	if (left && pos[0] > sizeObs && !hitCharacter (0) )
		pos[0] -= 1 * vel

	if (right && pos[0] + size < DIM[0] - sizeObs && !hitCharacter (1))
		pos[0] += 1 * vel

	if (up && pos[1] > sizeObs && !hitCharacter (2))
		pos[1] -= 1 * vel

	if (down && pos[1] + size < DIM[1] - sizeObs && !hitCharacter (3))
		pos[1] += 1 * vel
}

function hitCharacter (key) {

	switch (key) {
		case 0:
			return hitCHA_OBJ (0, 0, sizeObs, 1)
			break

		case 1:
			return hitCHA_OBJ (0, size, 0, 1)
			break

		case 2:
			return hitCHA_OBJ (1, 0, sizeObs, 0)
			break

		case 3:
			return hitCHA_OBJ (1, size, 0, 0)
			break
	}
	return false
}