var width
var height
var board

$(document).ready( function () {

	width = $('#board').width()
	height = $('#board').height()

	$(document).keydown( function (event) {
		getKey(event.key, true)
	})
	$(document).keyup( function (event) {
		getKey(event.key, false)
	})

	main ()
})

const X = 0
const Y = 1

const sDIM = 75
var floor

const speedH = 8
const speedV = -8
var jumpS = 0
const gravity = 0.2
var time

var up = false, objJump = false, fall = false, tfloor = false
var right = false, left = false

const inicio = 0
const final = 700

var pos
var obstacles

setInterval( function () {
	move ()
	drawObjects ()
	writeVariables ()
}, 10)

function getKey (key, action) {

	switch (key) {
		case "ArrowUp":
		case "w":
		case " ":
			up = action
			break

		case "ArrowRight":
		case "d":
			right = action
			break

		case "ArrowLeft":
		case "a":
			left = action
			break
	}
}

function move () {

	if ((up && !objJump && !fall) || (up && tfloor && !objJump)) {
		objJump = true
		fall = false
		time = 0
		jumpS = speedV
	}

	if ( right )
	//	pos[X] += speedH
		pos[X] += hit1 (sDIM, 0,  1, final)

	if ( left )
	//	pos[X] -= speedH
		pos[X] -= hit1 (0, sDIM, -1, inicio)
/*
	if (objJump) {
		pos[Y] -= hit2 (0, sDIM, -1, speedV)
		//pos[Y] -= speedV

		if (pos[Y] <= 0 || hit2 (0, sDIM, -1, speedV) == 0) {
		//if ( pos[Y] <= 0 ) { 
			objJump = false
			fall = true
		}
	}
	if (fall) {
		pos[Y] += hit2 (sDIM, 0, 1, speedV)

		if (pos[Y] >= floor - sDIM) {
			fall = false
			pos[Y] = floor - sDIM
		}

		if (hit2 (sDIM, 0, 1, speedV) == 0)
			tfloor = true
		else
			tfloor = false
	}
*/
	if (objJump) {
		jumpS += gravity
		pos[Y] += hit2 (0, sDIM, -1, Math.ceil(jumpS), 0)
		if (Math.floor(jumpS) >= 0 || hit2 (0, sDIM, -1, Math.floor(jumpS), 0) == 0) {
			objJump = false
			fall = true
			jumpS = 0
			tfloor = false
		}
	}
	if (fall) {
		if (!tfloor)
			jumpS += gravity
		pos[Y] += hit2 (sDIM, 0, 1, Math.ceil(jumpS), 1)
		if (hit2 (sDIM, 0, 1, Math.ceil(jumpS), 1) == 0)
			tfloor = true
		if (pos[Y] + sDIM >= floor) {
			fall = false
		}
	}

}

function jump (dir) {
	//var j = jumpS - gravity
	var j = jumpS
	j = 0
	return j * dir
	//return 4
}
function midTime () {
	return - speedV / gravity
}

function drawStage () {

	board.clearRect(0, 0, width, height)

	board.beginPath()
	// Suelo
	board.fillStyle = "lime"
	board.fillRect(0, floor, width, 50)
}

function drawObjects () {

	drawStage ()
/*
	// Circulo
	board.beginPath()
	board.fillStyle = "blue"
	board.arc(cPosX, cPosY, cDIM, 0, 2 * Math.PI)
	board.fill()
*/

	// Obstacles
	board.beginPath()
	board.fillStyle = "black"
	for (var i = 0; i < obstacles.length; i++)
		board.rect(obstacles[i][X], obstacles[i][Y], sDIM, sDIM)
	board.fill()

	// Cuadrado
	board.beginPath()
	board.fillStyle = "red"
	board.rect(pos[X], pos[Y], sDIM, sDIM)
	board.fill()
}

// Lateral crash
function hit1 (dimPlayer, dimObs, dir, wall) {
	for (var obs = 0; obs < obstacles.length; obs++) {
		for (var dim = 0; dim <= sDIM; dim++) {
			for (var s = 0; s <= speedH; s++) {
				if (
						(
							Math.floor(pos[0]) + dimPlayer + s * dir == obstacles[obs][0] + dimObs
							&&
							(
								Math.floor(pos[1]) == obstacles[obs][1] + dim
								||
								Math.floor(pos[1]) + sDIM == obstacles[obs][1] + dim
							)
						)
						||
						Math.floor(pos[0]) + dimPlayer + s * dir == wall
					)
					return s
			}
		}
	}
	return speedH
}

// Choque por abajo
function hit2 (dimPlayer, dimObs, dir, speed, hit) {
	for (var obs = 0; obs < obstacles.length; obs++) {
		for (var dim = 1 - 1 * hit; dim < sDIM + 1 * hit; dim++) {
			for (var s = 0; s <= Math.abs(speed); s++) {
				if (
						(
							(
								Math.floor(pos[X]) == obstacles[obs][X] + dim
								||
								Math.floor(pos[X]) + sDIM == obstacles[obs][X] + dim
							)
						&&
						Math.floor(pos[Y]) + dimPlayer + s * dir == obstacles[obs][Y] + dimObs
						)
						||
						(
							Math.floor(pos[Y]) + sDIM + s * dir == floor
							&&
							fall
						)
					)
					return s * dir
			}
		}
	}
	return speed
}

function main () {

	board = getID('board').getContext('2d')
	floor = height - 50
	pos = [inicio, floor-sDIM]
	obstacles = [
		//[sDIM * 1, floor - sDIM * 3],
		[sDIM * 4, floor - sDIM * 2],
		[sDIM * 5, floor - sDIM * 2],
		[sDIM * 3, floor - sDIM * 1],
		[sDIM * 5, floor - sDIM * 3],
		[sDIM * 6, floor - sDIM * 4],
		[sDIM * 3, floor - sDIM * 5],
		[sDIM * 7, floor - sDIM * 6],
	]

	document.addEventListener("keydown", function () { getKey(event, true) })
	document.addEventListener("keyup", function () { getKey(event, false) })
}

function getID (id) {
	return document.getElementById(id)
}

function writeVariables () {
	//$("#demo").html(up + " - " + objJump + " . " + fall + " -- " + Math.floor(pos[Y] + sDIM) + " - " + floor)
	$("#demo").html(Math.floor(jumpS) + " - " + Math.floor(pos[Y] + sDIM) + " - " + hit2(sDIM, 0, 1, Math.floor(jumpS)))
}

/*
	demo.innerHTML = "C" + Math.ceil(sPosX/sDIM) + " - F" + Math.floor(sPosX/sDIM) + " -- C"
		+ Math.ceil(sPosY/sDIM) + " - F" + Math.floor(sPosY/sDIM) + " -- C"
		+ Math.ceil((posBSquare[0][0]) / sDIM) + " - C" + Math.ceil((posBSquare[0][0] + sDIM) / sDIM) + " -- F"
		+ Math.floor((posBSquare[0][0]) / sDIM) + " - F" + Math.floor((posBSquare[0][0] + sDIM) / sDIM)
*/

/*
	
	Vy = V0y + g * T; 

	T = (Vy - V0y) / g   -->  T = 1.53

	Y = Y0 + V0y * T - 1/2 * g * T^2  --> Y = 11.479

	Y = Y0 + V0y * (Vy - V0y)/g - (Vy - V0y)^2 / 2 * g  -->  Y = V0y^2 / g - V0y / (2*g) --> Y = 0.765

	T = ( V0y + Math.sqrt( Math.pow(V0y, 2) + 2*g*Y ) ) / g
	T = ( V0y - Math.sqrt( Math.pow(V0y, 2) + 2*g*Y ) ) / g



	function cuadraticEcuation (a, b, c) {
		return (-b + Math.sqrt( Math.pow(b, 2) - (4 * a * c) ) ) / (2 * a);
	}
*/